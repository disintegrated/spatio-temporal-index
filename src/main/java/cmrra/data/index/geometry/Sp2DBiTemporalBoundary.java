package cmrra.data.index.geometry;

import cmrra.data.index.entity.*;
import cmrra.data.index.utility.ToolKit;
import com.vividsolutions.jts.geom.*;

import java.io.Serializable;

/**
 * Created by rabbiddog on 06.10.17.
 */
public class Sp2DBiTemporalBoundary implements Serializable {

    private SpatialAxisInterval _xRange;

    private SpatialAxisInterval _yRange;

    private TransactionTime _tRange;

    private ValidTime _vRange;

    private boolean _isHidden; //tracks if it was hidden entries

    public Sp2DBiTemporalBoundary(SpatialAxisInterval X, SpatialAxisInterval Y, TransactionTime T, ValidTime V)
    {
        //make sure the temporal dimensions make sense
        if(T.Deleted()){
            assert !V.isNowRelative(): "When transaction end is set then validity cannot be now relative";
            assert V.End() <= T.deletedAt(): "When transaction end is set then validity end cannot be greater than transaction end";
        }

        if(V.isNowRelative())
        {
            assert V.Start() <= T.entryAt() : "Valid start time cannot be greater than transaction start if the data is NOW relative";
        }

        this._xRange = X;
        this._yRange = Y;
        this._tRange =  T;
        this._vRange = V;
        this._isHidden = false;
    }

    public Sp2DBiTemporalBoundary(SpatialAxisInterval X, SpatialAxisInterval Y, TransactionTime T, ValidTime V, boolean isHidden)
    {
        this(X,Y,T, V);
        this._isHidden = isHidden;
    }

    public long delete()
    {
        if(!T().Deleted())
        {
            long time = T().deleteEntry();
            V().setUpperBoundTime(time); //sets the upper bound of only NOW relative data. Else unchanged
        }
        return T().deletedAt();
    }

    public boolean intersectsRectangle(Sp2DBiTemporalBoundary other)
    {
        //if any of the boundaries have a hidden child, that means that boundary's  V end should be max(current time, end time)
        //hidden will not be set for Valid end time NOW

        boolean vIntersects;

        if(!other.hasHidden() && !hasHidden())
            vIntersects = V().intersects(other.V());
        else
        {
            long thisS = V().Start();
            long thisE = hasHidden()? Long.max(V().currentStretch(), ToolKit.currentSeconds()): V().currentStretch();

            long otherS = other.V().Start();
            long otherE = hasHidden()? Long.max(other.V().currentStretch(), ToolKit.currentSeconds()): other.V().currentStretch();

            vIntersects = !(thisS > otherE || thisE < otherS);
        }

        return X().intersects(other.X()) && Y().intersects(other.Y()) && T().intersects(other.T()) && vIntersects;
    }

    public boolean intersectsRegion(Sp2DBiTemporalBoundary other)
    {
        return X().intersects(other.X()) && Y().intersects(other.Y()) && intersectsTemporalRegion(other);
    }

    public boolean intersectsTemporalRegion(Sp2DBiTemporalBoundary other)
    {
            Polygon polyThis = this.getTemporalBoundingRegion();
            Polygon polyOther = other.getTemporalBoundingRegion();

            return polyThis.intersects(polyOther);
    }

    public boolean intersectTemporalRectangle(Sp2DBiTemporalBoundary other)
    {
        return T().intersects(other.T()) && V().intersects(other.V());
    }

    public boolean intersectsSpatialRegion(Sp2DBiTemporalBoundary other)
    {
        return X().intersects(other.X()) && Y().intersects(other.Y());
    }
    /**
     * provides the Hyperrectangle equivalent of area of rectangle in a 2D space
     * calculate the volume
     * @return
     */
    public double areaEquivRectangle()
    {
        float validStretch = V().currentStretch()- V().Start();
        float transactionStretch = T().currentStretch() - T().entryAt();

        return (X().end - X().start) * (Y().end - Y().start) * validStretch * transactionStretch;
    }

    /**
     * calculate the volume of the intersected space
     * @return
     */
    public double areaEquivIntersectRectangle(Sp2DBiTemporalBoundary other)
    {
        if(!intersectsRectangle(other))
            return 0;
        else
        {
            DoubleInterval x = new DoubleInterval(Math.max(X().start, other.X().start), Math.min(X().end, other.X().end));
            DoubleInterval y = new DoubleInterval(Math.max(Y().start, other.Y().start), Math.min(Y().end, other.Y().end));
            DoubleInterval t = new DoubleInterval(Math.max(T().entryAt(), other.T().entryAt()), Math.min(T().currentStretch(), other.T().currentStretch()));
            DoubleInterval v = new DoubleInterval(Math.max(V().Start(), other.V().Start()), Math.min(V().currentStretch(), other.V().currentStretch()));

            return x.interval() * y.interval() * t.interval() * v.interval();
        }
    }

    /**
     * provides the surface area of the volume
     * @return
     */
    public double marginEquivRectangle()
    {
        DoubleInterval x = new DoubleInterval(X().start, X().end);
        DoubleInterval y = new DoubleInterval(Y().start, Y().end);
        DoubleInterval t = new DoubleInterval(T().entryAt(), T().currentStretch());
        DoubleInterval v = new DoubleInterval(V().Start(), V().currentStretch());

        double vol1 = x.interval()*y.interval()*t.interval();
        double vol2 = x.interval()*y.interval() *v.interval();
        double vol3 = v.interval()*y.interval()*t.interval();
        double vol4 = v.interval()*x.interval()*t.interval();

        return vol1*2 + vol2*2 +vol3*2 + vol4*2;
    }

    public Polygon getTemporalBoundingRegion()
    {
        assert T() != null : "Must have a Transaction Time boundary defined";
        assert V() != null : "Miust have a Valid Time boundary defined";

        long time = ToolKit.currentSeconds();

        CoordinateList vertices = new CoordinateList();

        vertices.add(new Coordinate(T().entryAt(), V().Start()));

        if(V().startedAsNowRelative())
        {
            if(V().Start() < T().entryAt())
            {
                //initial jump. This is for data with older valid start time entered in DB later on
                //case5, case6
                vertices.add(new Coordinate(T().entryAt(), T().entryAt()));
            }
            //will follow a staircase structure. Approximate as a slant line
            if (V().isNowRelative())
            {
                //case3, case5
                vertices.add(new Coordinate(time, time));
                vertices.add(new Coordinate(time, V().Start()));
                vertices.add(new Coordinate(T().entryAt(), V().Start()));
            }
            else
            {
                //at some point the validity end was set or the transaction end was set
                //case4, case6
                vertices.add(new Coordinate(V().End(), V().End()));
                vertices.add(new Coordinate(V().End(), V().Start()));
                vertices.add(new Coordinate(T().entryAt(), V().Start()));

            }
        }
        else
        {
            if(T().deletedAt() == Constants.TRANSACTION_UC)
            {
                //case1
                vertices.add(new Coordinate(T().entryAt(), V().End()));
                vertices.add(new Coordinate(time, V().End()));
                vertices.add(new Coordinate(time, V().Start()));
                vertices.add(new Coordinate(T().entryAt(), V().Start()));
            }
            else
            {
                //case2
                vertices.add(new Coordinate(T().entryAt(), V().End()));
                vertices.add(new Coordinate(T().deletedAt(), V().End()));
                vertices.add(new Coordinate(T().deletedAt(), V().Start()));
                vertices.add(new Coordinate(T().entryAt(), V().Start()));
            }
        }

        Polygon poly = ToolKit.GEOMETRY_FACTORY.createPolygon(vertices.toCoordinateArray());
        return poly;
    }

    public SpatialAxisInterval X() {
        return _xRange;
    }

    public SpatialAxisInterval Y() {
        return _yRange;
    }

    public TransactionTime T() {
        return _tRange;
    }

    public ValidTime V() {
        return _vRange;
    }

    public boolean hasHidden()
    {
        return _isHidden;
    }

    public Sp2DBiTemporalBoundary union(Sp2DBiTemporalBoundary other)
    {
        SpatialAxisInterval x = _xRange.union(other._xRange);
        SpatialAxisInterval y = _yRange.union(other._yRange);
        ValidTime v= _vRange.union(other._vRange);
        TransactionTime  t = _tRange.union(other._tRange);

        return new Sp2DBiTemporalBoundary(x, y, t, v);
    }

    public Sp2DBiTemporalBoundary resetTransactionTime()
    {
        TransactionTime t = new TransactionTime();
        return new Sp2DBiTemporalBoundary(_xRange, _yRange, t, _vRange);
    }
}
