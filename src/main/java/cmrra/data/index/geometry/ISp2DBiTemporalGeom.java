package cmrra.data.index.geometry;

import cmrra.data.index.entity.SpatialAxisInterval;
import cmrra.data.index.entity.TransactionTime;
import cmrra.data.index.entity.ValidTime;

/**
 * Created by rabbiddog on 14.10.17.
 */
public interface ISp2DBiTemporalGeom {

    SpatialAxisInterval X();
    SpatialAxisInterval Y();
    TransactionTime T();
    ValidTime V();
    Sp2DBiTemporalBoundary BB();
    boolean hasHidden();

}
