package cmrra.data.index.grstar;

/**
 * Created by rabbiddog on 24.10.17.
 */
public final class Operation {

    public final OPERATOR operator;

    public final FIELD field;

    public final long value;

    public Operation(OPERATOR operator, FIELD field, long value)
    {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }
}
