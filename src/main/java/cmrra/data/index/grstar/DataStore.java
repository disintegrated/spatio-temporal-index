package cmrra.data.index.grstar;

import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.util.HashSet;
import java.util.List;
import com.google.common.base.Optional;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by rabbiddog on 03.10.17.
 */
public interface DataStore<V> {

    void add(Spatial2DBiTemporalData<V> SpT) throws Exception;

    Observable<Spatial2DBiTemporalData<V>> search(Sp2DBiTemporalBoundary boundary);

    Observable<Spatial2DBiTemporalData<V>> searchWithOperator(Operation op) throws Exception;

    int softDelete(Spatial2DBiTemporalData<V> data) throws Exception;

    int deletePermanent(Spatial2DBiTemporalData<V> data) throws Exception;

    Single<Optional<Spatial2DBiTemporalData<V>>> searchByID(V id);

    Observable<Pair<V, Optional<Spatial2DBiTemporalData<V>>>> searchByIDs(HashSet<V> ids);

    int size();

    Observable<Spatial2DBiTemporalData<V>> streamAll();

    String getName();
}
