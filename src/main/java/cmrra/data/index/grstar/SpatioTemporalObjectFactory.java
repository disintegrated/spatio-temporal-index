package cmrra.data.index.grstar;

import cmrra.data.index.entity.*;
import cmrra.data.index.utility.ToolKit;
import com.vividsolutions.jts.geom.Geometry;


/**
 * Created by rabbiddog on 03.10.17.
 */
public final class SpatioTemporalObjectFactory {

    private SpatioTemporalObjectFactory(){}

    public static Spatial2DBiTemporalNowRelativeBinaryData createSpatial2DBiTemporalNowRelativeData(Geometry geom, long validFrom, boolean destinationIn, byte[] data)
    {
        ValidTime vt = new ValidTime(validFrom);
        SpatialObject2D sb2d = new SpatialObject2D(geom);

        TransactionTime tt = new TransactionTime();

        Spatial2DBiTemporalNowRelativeBinaryData spatioTemporalObject = new Spatial2DBiTemporalNowRelativeBinaryData(ToolKit.getUUID(), sb2d.getXRange(), sb2d.getYRange(), tt, vt, destinationIn, data);

        return spatioTemporalObject;
    }

    public static Spatial2DBiTemporalNowRelativeBinaryData createSpatial2DBiTemporalNowRelativeData(Geometry geom, long validFrom, long validTo, boolean destinationIn, byte[] data)
    {
        ValidTime vt = new ValidTime(validFrom, validTo);
        SpatialObject2D sb2d = new SpatialObject2D(geom);

        TransactionTime tt = new TransactionTime();

        Spatial2DBiTemporalNowRelativeBinaryData spatioTemporalObject = new Spatial2DBiTemporalNowRelativeBinaryData(ToolKit.getUUID(), sb2d.getXRange(), sb2d.getYRange(),tt, vt, destinationIn, data);

        return spatioTemporalObject;
    }
}
