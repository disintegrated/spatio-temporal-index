package cmrra.data.index.grstar;

/**
 * Created by rabbiddog on 24.10.17.
 */
public enum OPERATOR {
    LESS_THAN,
    GREATER_THAN
}
