package cmrra.data.index.grstar;

/**
 * Created by rabbiddog on 03.10.17.
 */
import cmrra.data.index.entity.*;
import cmrra.data.index.factories.Geometryfactory;
import cmrra.data.index.factories.INodeFactory;
import cmrra.data.index.factories.NodeFactoryDefault;
import cmrra.data.index.factories.StoreFactory;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.store.IStore;
import cmrra.data.index.utility.*;
import com.google.common.base.Optional;
import io.reactivex.*;
import io.reactivex.Observable;
import org.apache.commons.lang3.tuple.*;
import java.util.*;

/**
 * GR tree supporting both temporal and spatial indexes
 * The following tree implementation works with Spatio-Temporal data that uses a string as identifier
 * and not with a generic spatiotemporal object because of the specific design of the Index
 */
public class ExtendedGRTree implements DataStore<String>{

    static{
        Setup.setupLogging("log.out");
    }

    /**
     * Benchmarks show that this is a good choice for up to O(10,000) entries
     * when using Quadratic splitter (Guttman).
     */
    public static final int MAX_CHILDREN_DEFAULT_GUTTMAN = 4;

    /**
     * Benchmarks show that this is the sweet spot for up to O(10,000) entries
     * when using R*-tree heuristics.
     */
    public static final int MAX_CHILDREN_DEFAULT_STAR = 4;


    protected Sp2DBiTNowRelNode<String> root;
    protected final Context context;

    private ExtendedGRTree(Context context)
    {
        this.context = context;
    }

    public static ExtendedGRTree create(String databaseName)
    {
        return new Builder().create(databaseName);
    }

    /**
     * When the number of children in an R-tree node drops below this number the
     * node is deleted and the children are added on to the R-tree again.
     *
     * @param minChildren
     *            less than this number of children in a node triggers a node
     *            deletion and redistribution of its members
     * @return builder
     */
    public static Builder minChildren(int minChildren) {
        return new Builder().minChildren(minChildren);
    }

    /**
     * Sets the max number of children in an R-tree node.
     *
     * @param maxChildren
     *            max number of children in an R-tree node
     * @return builder
     */
    public static Builder maxChildren(int maxChildren) {
        return new Builder().maxChildren(maxChildren);
    }


    public static class Builder{

        private static final double DEFAULT_FILLING_FACTOR = 0.4;
        private Optional<Integer> maxChildren = Optional.absent();
        private Optional<Integer> minChildren = Optional.absent();

        public ExtendedGRTree create(String databaseName)
        {
            /*if(cls == null || Spatial2DBiTemporalNowRelativeBinaryData.class != cls)
                throw new IllegalArgumentException("Only Spatial2DBiTemporalNowRelativeBinaryData is supported as of yet");*/
            setDefaultCapacity();
            ISplitter splitter = new SplitterRStar();
            INodeFactory nodeFactory = new NodeFactoryDefault();
            ISelector selector = new SelectorRStar();
            IStore store = StoreFactory.createNamedMapDBBackedStore(databaseName);
            Configuration configuration = new Configuration();
            configuration.setDBName(databaseName);
            ToolKit.init(configuration);
            Context context = new Context(minChildren.get(), maxChildren.get(), splitter, nodeFactory, selector, store, configuration);


            return new ExtendedGRTree(context);
        }

        /**
         * When the number of children in an R-tree node drops below this number
         * the node is deleted and the children are added on to the R-tree
         * again.
         *
         * @param minChildren
         *            less than this number of children in a node triggers a
         *            redistribution of its children.
         * @return builder
         */
        public Builder minChildren(int minChildren) {
            this.minChildren = Optional.of(minChildren);
            return this;
        }

        /**
         * Sets the max number of children in an R-tree node.
         *
         * @param maxChildren
         *            max number of children in R-tree node.
         * @return builder
         */
        public Builder maxChildren(int maxChildren) {
            this.maxChildren = Optional.of(maxChildren);
            return this;
        }


        private void setDefaultCapacity() {
            if (!maxChildren.isPresent())
                    maxChildren = Optional.of(MAX_CHILDREN_DEFAULT_STAR);
            if (!minChildren.isPresent())
                minChildren = Optional.of((int) Math.round(maxChildren.get() * DEFAULT_FILLING_FACTOR));
        }
    }

    @Override
    public void add(Spatial2DBiTemporalData<String> data) throws Exception
    {
        try
        {
            if(null == root)
            {
                root = context.nodeFactory().createLeaf(context.store(), context);
            }
            //set the transaction time.//do not rely on validity of incoming data
            if(data.isDeleted())
                throw new IllegalArgumentException("Data is marked as deleted. Cannot add to store");
            data.resetTransactionTime();
            Pair<? extends Node<String>, ? extends Node<String>> finalNodes= root.add(data);
            if(null != finalNodes.getRight())
            {
                List<Node<String>> children = new ArrayList<>();
                children.add(finalNodes.getLeft());
                children.add(finalNodes.getRight());
                root = context.nodeFactory().createNonLeaf(context.store(), children, context);
            }
        }
        catch (Exception ex)
        {
            Log.info("Operation Add to Tree failed beacuse of internal error");
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }

    }


    public Observable<Spatial2DBiTemporalData<String>> search(io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion) {
       if(null == root)
           return Observable.empty();
        return Observable.create(new ObservableOnStore<String>(root, criterion));
    }

    @Override
    public Observable<Spatial2DBiTemporalData<String>> search(Sp2DBiTemporalBoundary boundary)
    {
        return search(intersects(boundary));
    }

    @Override
    public Observable<Spatial2DBiTemporalData<String>> searchWithOperator(Operation op) throws Exception
    {
        switch (op.field)
        {
            case T_START:
                if(op.operator == OPERATOR.GREATER_THAN)
                    return Observable.create(new ObservableOnStore<String>(root, afterTransactionTime(op.value)));
                else if(op.operator == OPERATOR.LESS_THAN)
                    throw new IllegalArgumentException("This operation is not defined yet and hence not allowed");
                break;
            case V_START:
                throw new IllegalArgumentException("This operation is not defined yet and hence not allowed");

            case V_END:
                throw new IllegalArgumentException("This operation is not defined yet and hence not allowed");

        }
        throw new IllegalArgumentException("This operation is not defined yet and hence not allowed");
    }


    @Override
    public int softDelete(Spatial2DBiTemporalData<String> data) throws Exception
    {
        try
        {
            if(null == root)
                return 0;
           return root.deleteSoft(data);
        }catch (Exception ex)
        {
            Log.info("Operation softDelete on Tree failed beacuse of internal error");
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public int deletePermanent(Spatial2DBiTemporalData<String> data) throws Exception {
        try{
            if(null == root)
                return 0;
            if(Leaf.class.isAssignableFrom(root.getClass()))
            {
                NodeAndEntries<String> result = root.deletePermanent(data);
                return result.countDeleted();
            }else //root is allowed to have less than minChildren
            {
                NodeAndEntries<String> result = root.deletePermanent(data);

                int level = result.level() +1;
                //insert leaf entries
                HashSet<Node<String>>  leaves = result.Q().get(0);
                HashMap<String, Spatial2DBiTemporalData<String>> entries = new HashMap<>();
                if(null != leaves)
                {
                    for (Node<String> n:leaves)
                    {
                        Leaf<String> l = (Leaf<String>)n;
                        HashMap<String, Spatial2DBiTemporalData<String>> lentries =  root.context().store().get(((Leaf<String>) n).entries());
                        lentries.forEach(entries :: putIfAbsent);
                    }
                }
                for (Map.Entry<String, Spatial2DBiTemporalData<String>> e: entries.entrySet())
                {
                    this.add(e.getValue());
                }

                //rest of the nodes
                for (Map.Entry<Integer, HashSet<Node<String>>> e:result.Q().entrySet())
                {
                    NonLeaf<String> _root = (NonLeaf<String>)root;
                    if(e.getKey() == 0)
                        continue;
                    HashSet<Node<String>>  nodes  = e.getValue();
                    for (Node<String> n: nodes)
                    {
                        Pair<? extends Node<String>, ? extends Node<String>> finalNodes=  _root.addNode(level, new ImmutablePair<Integer, Node<String>>(e.getKey(), n));
                        if(null != finalNodes.getRight())
                        {
                            List<Node<String>> children = new ArrayList<>();
                            children.add(finalNodes.getLeft());
                            children.add(finalNodes.getRight());
                            root = context.nodeFactory().createNonLeaf(context.store(), children, context);
                            level +=1; //since with the new root creating the root moved up a level
                        }
                    }
                }

                //check if root has only 1 child.If yes then create new root
                if(NonLeaf.class.isAssignableFrom(root.getClass()))
                {
                    NonLeaf<String> _root = (NonLeaf<String>)root;
                    if(_root.children().size() == 1)
                        root = (Sp2DBiTNowRelNodeNonLeaf)(_root.children().get(0));
                }

                return result.countDeleted();
            }
        }catch (Exception ex)
        {
            Log.info("Operation deletePermanent on Tree failed beacuse of internal error");
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public Single<Optional<Spatial2DBiTemporalData<String>>> searchByID(String id) {
        Single<Optional<Spatial2DBiTemporalData<String>>> tSingle = Single.create(new SingleOnSubscribe<Optional<Spatial2DBiTemporalData<String>>>() {
            @Override
            public void subscribe(SingleEmitter<Optional<Spatial2DBiTemporalData<String>>> e) throws Exception {
                if(null == root)
                {
                    e.onError(new NullPointerException("Root node has not been initialized"));
                }else
                {
                    IStore<String> store = root.context().store();
                    Spatial2DBiTemporalData<String> data = store.get(id);
                    if(null != data)
                        e.onSuccess(Optional.<Spatial2DBiTemporalData<String>>of(data));
                    else
                        e.onSuccess(Optional.absent());
                }
            }
        });

        return tSingle;
    }

    @Override
    public Observable<Pair<String, Optional<Spatial2DBiTemporalData<String>>>> searchByIDs(HashSet<String> ids) {
        Observable<Pair<String, Optional<Spatial2DBiTemporalData<String>>>> tObservable = Observable.create(new ObservableOnSubscribe<Pair<String, Optional<Spatial2DBiTemporalData<String>>>>() {
            @Override
            public void subscribe(ObservableEmitter<Pair<String, Optional<Spatial2DBiTemporalData<String>>>> e) throws Exception {
                if(null == ids)
                {
                    e.onError(new NullPointerException("null parameter"));
                }
                if(null == root)
                {
                    e.onError(new NullPointerException("Root node has not been initialized"));
                }
                else
                {
                    IStore<String> store = root.context().store();
                    for (String id :ids)
                    {
                        try{
                            Spatial2DBiTemporalData<String> data = store.get(id);
                            if(null != data)
                                e.onNext(new ImmutablePair<>(id, Optional.of(data)));
                            else
                                e.onNext(new ImmutablePair<>(id, Optional.absent()));
                        }catch (Exception ex)
                        {
                            e.onError(ex);
                        }
                    }
                }
            }
        });

        return tObservable;
    }

    @Override
    public int size() {
        if(null == root)
            return 0;
        else {
            return root.context().store().size();
        }
    }

    @Override
    public Observable<Spatial2DBiTemporalData<String>> streamAll() {
        if(null == root)
            return Observable.empty();
        else
            return root.context().store().streamAll();
    }

    @Override
    public String getName() {
        return context.store().getName();
    }

    private static io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> intersects(Sp2DBiTemporalBoundary boundary)
    {
        return new io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean>(){

            @Override
            public Boolean apply(Sp2DBiTemporalBoundary dataBB) throws Exception {
                return dataBB.intersectsRectangle(boundary);
            }
        };
    }

    private static io.reactivex.functions.BiFunction<Sp2DBiTemporalBoundary, Class<? extends Object>, Boolean> afterTransactionTime(long t)
    {
        return new io.reactivex.functions.BiFunction<Sp2DBiTemporalBoundary, Class<? extends Object>, Boolean>(){

            @Override
            public Boolean apply(Sp2DBiTemporalBoundary boundary, Class<? extends Object> objectClass) throws Exception {
                if(Sp2DBiTNowRelNode.class.isAssignableFrom(objectClass))
                {
                    if(boundary.T().currentStretch() < t)
                        return false;
                    else
                        return true;
                }
                else if(Spatial2DBiTemporalData.class.isAssignableFrom(objectClass))
                {
                    if(boundary.T().entryAt() < t)
                        return false;
                    else
                        return true;
                }
                else { //fail safe
                    Sp2DBiTemporalBoundary bb = Geometryfactory.createBoundary(0, Double.MAX_VALUE, 0, Double.MAX_VALUE, t, Long.MAX_VALUE, 0, Long.MAX_VALUE);
                    return bb.intersectsRectangle(boundary);
                }
            }
        };
    }
}
