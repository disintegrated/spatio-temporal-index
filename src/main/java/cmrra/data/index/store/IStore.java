package cmrra.data.index.store;

import cmrra.data.index.entity.Spatial2DBiTemporalData;
import io.reactivex.Observable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by rabbiddog on 07.10.17.
 * T: data type
 * V: key type
 */
public interface IStore<V> {
    Spatial2DBiTemporalData<V> get(V UUID)throws Exception;
    HashMap<V, Spatial2DBiTemporalData<V>> get(HashSet<V> UUIDs)throws Exception;
    Spatial2DBiTemporalData<V> put(Spatial2DBiTemporalData<V> data)throws Exception;
    Spatial2DBiTemporalData<V> remove(V UUID)throws Exception;
    Spatial2DBiTemporalData<V> hardDelete(V UUID)throws Exception;
    int size();
    Observable<Spatial2DBiTemporalData<String>> streamAll();
    String getName();
}
