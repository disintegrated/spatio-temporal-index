package cmrra.data.index.store;

import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.utility.Log;
import cmrra.data.index.utility.Spatial2DBiTemporalDataSerializer;
import io.reactivex.Observable;
import org.mapdb.*;
import io.reactivex.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by rabbiddog on 04.10.17.
 */

/**
 * Store for Spatio-Temporal data
 * Since data sizes can be large, a complete memory based solution would be infeasable for resource intensive hardware
 */
public class MapDBBackedStore implements IStore<String>{

    private static MapDBBackedStore _instance;
    private DB _db;
    private String dbFile;
    private Spatial2DBiTemporalDataSerializer serializer;

    private HTreeMap<String, byte[]> _map;

    public MapDBBackedStore()
    {
        serializer = new Spatial2DBiTemporalDataSerializer();
    }

    /**
     * Use the file name provided to persist the data
     * @param storeName the name of the file to store the data.
     * @param append true: use the existing store if already present
     *               false:delete old store and create a new one
     */
    public void Init(String storeName, boolean append)
    {
        dbFile = storeName+".db";
        File store = new File(dbFile);

        if(!append)
        {
            try
            {
                if(Files.exists(store.toPath())){

                    Files.delete(store.toPath());
                }
            }catch (IOException ioex)
            {
                Log.error(Log.getExceptionString(ioex));
            }
        }
        createStore();
    }

    private void createStore()
    {
        File store = new File(dbFile);
        try{
            if(null != _db)
            {
                _db.close();
            }
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));

        }finally {
            _db = null;
        }
        try
        {
            _db = DBMaker.fileDB(store).make();
            _map = _db.hashMap("store")
                    .keySerializer(Serializer.STRING)
                    .valueSerializer(Serializer.BYTE_ARRAY)
                    .create();



        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public Spatial2DBiTemporalData<String> get(String UUID)throws Exception
    {
        try
        {
            if(null == _db)
                createStore();

            byte[] obj = _map.get(UUID);
            Spatial2DBiTemporalData<String> result = serializer.deserialize(obj);
            if(null == result)
                Log.info("No data found by ID: "+UUID);

            return result;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public HashMap<String, Spatial2DBiTemporalData<String>> get(HashSet<String> UUIDs)throws Exception
    {
        HashMap<String, Spatial2DBiTemporalData<String>> result = new HashMap<>();
        try
        {
            if(null == _db)
                createStore();

            for (String id:UUIDs)
            {
                try
                {
                    byte[] obj = _map.get(id);
                    result.put(id, serializer.deserialize(obj));
                }catch (Exception inEx)
                {
                    Log.error("No data found by ID: "+id);
                }
            }
            return result;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    /**
     * @param data
     * @return previous value associated with the key if present
     *          else null
     */
    @Override
    public Spatial2DBiTemporalData<String> put(Spatial2DBiTemporalData<String> data)throws Exception
    {
        try
        {
            if(null == _db)
                createStore();

            byte[] obj = serializer.serialize(data);
            byte[] prev = _map.put(data.UUID, obj);


            _db.commit();

            if(null == prev)
                return null;
            else
                return serializer.deserialize(prev);
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            _db.rollback();
            throw ex;
        }
    }

    /**
     * Removes the value associated w
     * @param UUID the id of the data to be deleted
     * @return previous value associated with the key,
     *          else null
     */
    @Override
    public Spatial2DBiTemporalData<String> remove(String UUID)throws Exception
    {
        try{
            Spatial2DBiTemporalData<String> data = get(UUID);
            if(null == data)
                return null;
            data.delete(); //sets the time boundary
            hardDelete(UUID); //not sure if this is required
            put(data);
            _db.commit();
            return data;
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            _db.rollback();
            throw ex;
        }
    }

    @Override
    public Spatial2DBiTemporalData<String> hardDelete(String UUID)throws Exception
    {
        try
        {
            if(null == _db)
                createStore();

            byte[] obj = _map.remove(UUID);
            _db.commit();
            if(null == obj)
                return null;
            else
                return serializer.deserialize(obj);
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            _db.rollback();
            throw ex;
        }
    }

    @Override
    public int size()
    {
        try{
            if(null == _db)
                createStore();
            return _map.size();
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public Observable<Spatial2DBiTemporalData<String>> streamAll()
    {
            if(null == _db || null == _map)
                return Observable.empty();

            Observable<Spatial2DBiTemporalData<String>> observable =
                    Observable.create(new ObservableOnSubscribe<Spatial2DBiTemporalData<String>>() {
                        @Override
                        public void subscribe(ObservableEmitter<Spatial2DBiTemporalData<String>> e) throws Exception {

                            try{
                                for (Map.Entry<String, byte[]> entry: _map.getEntries())
                                {
                                    Spatial2DBiTemporalData<String> data = serializer.deserialize(entry.getValue());
                                    e.onNext(data);
                                }
                                e.onComplete();
                            }catch (Exception ex)
                            {
                                Log.error(Log.getExceptionMessage(ex));
                                e.onError(ex);
                            }
                        }
                    });

            return observable;
    }

    @Override
    public String getName() {
        return dbFile;
    }
}
