package cmrra.data.index.entity;

import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.utility.Context;
import cmrra.data.index.utility.Sp2DBiTNowRelNodeNonLeafHelper;
import cmrra.data.index.store.IStore;
import cmrra.data.index.utility.Log;
import cmrra.data.index.utility.ToolKit;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.BiFunction;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rabbiddog on 04.10.17.
 */
public class Sp2DBiTNowRelNodeNonLeaf<V extends Serializable> extends Sp2DBiTNowRelNode<V> implements NonLeaf<V> {

    private List<Node<V>> _children;

    private final IStore<V> _store;

    private final Context _context;

    private boolean _hidden; //value required by Now-Relative Bi-Temporal axis

    private boolean _rectangle; //value required by Now-Relative Bi-Temporal axis

    public Sp2DBiTNowRelNodeNonLeaf(long id, IStore<V> store, Context context)
    {
        super(id);
        _store = store;
        _context = context;
        _children = new ArrayList<>();
    }

    public Sp2DBiTNowRelNodeNonLeaf(long id, IStore<V> store, Context context, List<Node<V>> children) throws Exception
    {
        super(id);
        _store = store;
        _context =context;
        _children = children;
        if(_children.size() > 0)
            updateBoundary();

    }

    @Override
    public List<Node<V>> children() {
        if(null == _children)
        {
            _children = new ArrayList<>();
        }

        return _children;
    }

    @Override
    public void setChildren(List<Node<V>> c)
    {
        _children = c;
    }

    @Override
    public IStore<V> store() {
        return _store;
    }

    @Override
    public Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> addNode(int level, Pair<Integer, Node<V>> entry)throws Exception
    {
        try{
            Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> finalNodes = Sp2DBiTNowRelNodeNonLeafHelper.addNode(level, entry, this);
            if(null == finalNodes.getRight())
            {
                updateBoundary();
            }
            else
            {
                Sp2DBiTNowRelNode.removeUniqueId(this.nodeId);
            }

            return finalNodes;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public SpatialAxisInterval X() {
        return _mbRegion.X();
    }

    @Override
    public SpatialAxisInterval Y() {
        return _mbRegion.Y();
    }

    @Override
    public TransactionTime T() {
        return _mbRegion.T();
    }

    @Override
    public ValidTime V() {
        return _mbRegion.V();
    }

    @Override
    public Sp2DBiTemporalBoundary BB() {
        return _mbRegion;
    }

    @Override
    public boolean hasHidden() {
        return _mbRegion.hasHidden();
    }

    @Override
    public Pair<? extends Node<V>, ? extends Node<V>> add(Spatial2DBiTemporalData<V> data) throws Exception
    {
        try{

            Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> finalNodes = Sp2DBiTNowRelNodeNonLeafHelper.add(data, this);

            if(null == finalNodes.getRight())
            {
                updateBoundary();
            }
            else
            {
                Sp2DBiTNowRelNode.removeUniqueId(this.nodeId);
            }

            return finalNodes;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public int deleteSoft(Spatial2DBiTemporalData<V> data) throws Exception {
        try{

            return Sp2DBiTNowRelNodeNonLeafHelper.deleteSoft(data, this);
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public NodeAndEntries<V> deletePermanent(Spatial2DBiTemporalData<V> data) throws Exception {
        try{
            NodeAndEntries<V> result = Sp2DBiTNowRelNodeNonLeafHelper.deletePermanent(data, this);

            if(result.node().isPresent() && result.countDeleted() > 0)
                updateBoundary();

            return result;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    @Override
    public void search(io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter)
    {
        Sp2DBiTNowRelNodeNonLeafHelper.search(criterion, emitter, this);
    }

    @Override
    public void search(BiFunction<Sp2DBiTemporalBoundary, Class<?>, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter)
    {
        Sp2DBiTNowRelNodeNonLeafHelper.search(criterion, emitter, this);
    }

    @Override
    public Context context() {
        return _context;
    }

    @Override
    protected void updateBoundary() throws Exception
    {
        _mbRegion = ToolKit.getMinimumBoundingRegion(_children);
    }
}
