package cmrra.data.index.entity;

/**
 * Created by rabbiddog on 03.10.17.
 */
public final class Constants {

    public static final long VALID_NOW = -1001;
    public static final long TRANSACTION_UC = -1002;
}
