package cmrra.data.index.entity;

import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;

import java.io.Serializable;

/**
 * Created by rabbiddog on 03.10.17.
 * An example spatio temporal application data
 *
 */
public class Spatial2DBiTemporalNowRelativeBinaryData extends Spatial2DBiTemporalData<String> implements Serializable {

    private byte[] _data;

    /*private SpatialObject2D _spatial;

    private ValidTime _validTime;

    private TransactionTime _transactionTime;*/

    /**
     * true if the data must travel to the space defined in _spatial
     * false if the data originated at the space defined in _spatial
     */
    private final boolean _destinationInward;

    public Spatial2DBiTemporalNowRelativeBinaryData(String UUID,SpatialAxisInterval x, SpatialAxisInterval y, TransactionTime tt, ValidTime vt, boolean destinationInward, byte[] data)
    {
        super(UUID, new Sp2DBiTemporalBoundary(x,y,tt,vt));
        _data = data;
        this._destinationInward = destinationInward;
    }

    @Override
    public SpatialAxisInterval X() {
        return _boundary.X();
    }

    @Override
    public SpatialAxisInterval Y() {
        return _boundary.Y();
    }

    @Override
    public TransactionTime T() {
        return _boundary.T();
    }

    @Override
    public ValidTime V() {
        return _boundary.V();
    }

    @Override
    public Sp2DBiTemporalBoundary BB() {
        return _boundary;
    }

    @Override
    public boolean hasHidden() {
        return _boundary.hasHidden();
    }

    @Override
    public void delete()
    {
        _boundary.delete(); //sets the transaction and valid time
        _data = new byte[0];
        _deleted = true;
    }
}
