package cmrra.data.index.entity;

import cmrra.data.index.store.IStore;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.HashSet;

/**
 * Created by rabbiddog on 09.10.17.
 */
public interface Leaf<V extends Serializable> extends Node<V> {

    HashSet<V> entries();

    IStore<V> store();
}
