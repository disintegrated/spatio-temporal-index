package cmrra.data.index.entity;

/**
 * Created by rabbiddog on 03.10.17.
 * Captures the spatial data
 * Types allowed are Point, Polygon, LineString
 */
import com.vividsolutions.jts.geom.*;

import java.io.Serializable;

public class SpatialObject2D implements Serializable{

    private final SpatialAxisInterval _X;

    private final SpatialAxisInterval _Y;

    private final Geometry _geometry;

    public SpatialObject2D(Geometry geometry)
    {
        assert geometry instanceof Point || geometry instanceof Polygon || geometry instanceof LineString;

        this._geometry = geometry;
        Envelope env = this._geometry.getEnvelopeInternal();
        this._X = new SpatialAxisInterval(env.getMinX(), env.getMaxX());
        this._Y = new SpatialAxisInterval(env.getMinY(), env.getMaxY());
    }

    public SpatialAxisInterval getXRange()
    {
        return _X;
    }

    public SpatialAxisInterval getYRange()
    {
        return _Y;
    }

    public boolean doesMBROverlap(SpatialObject2D obj)
    {
        // If one rectangle is on left side of other
        if (this.getXRange().start > obj.getXRange().end || obj.getXRange().start > this.getXRange().end)
            return false;

        // If one rectangle is above other
        if (this.getYRange().start < obj.getYRange().end || obj.getYRange().start < this.getYRange().end)
            return false;

        return true;
    }
}
