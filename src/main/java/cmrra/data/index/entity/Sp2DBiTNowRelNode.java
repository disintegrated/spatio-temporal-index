package cmrra.data.index.entity;

import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by rabbiddog on 04.10.17.
 */
public abstract class Sp2DBiTNowRelNode<V extends Serializable> implements Node<V>{

    protected Sp2DBiTemporalBoundary _mbRegion;

    private static HashSet<Long> usedIds = new HashSet<>();
    private static Random rng = new Random();

    public final long nodeId;

    public Sp2DBiTNowRelNode(long id)
    {
        this.nodeId = id;
        usedIds.add(id);
    }

    protected abstract void updateBoundary() throws Exception;

    public static long getUniqueNodeId()
    {
        long candidate = 0 ;
        do{
            candidate = rng.nextLong();
        }while (usedIds.contains(candidate));
        return candidate;
    }

    public static void removeUniqueId(long nodeId)
    {
        usedIds.remove(nodeId);
    }

    @Override
    public boolean equals(Object obj)
    {
        if(null == obj)
            return false;
        Sp2DBiTNowRelNode other = (Sp2DBiTNowRelNode)obj;
        return other.nodeId == this.nodeId;
    }
}
