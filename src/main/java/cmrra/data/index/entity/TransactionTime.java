package cmrra.data.index.entity;

import cmrra.data.index.utility.ToolKit;

import java.io.Serializable;

/**
 * Created by rabbiddog on 03.10.17.
 */
public class TransactionTime implements Serializable {

    private long _entryAt; //in seconds since  UTC 1/1/1970 epoch
    private long _deletedAt; //in seconds since  UTC 1/1/1970 epoch

    private boolean _deleted;

    public TransactionTime()
    {
        _entryAt = ToolKit.currentSeconds();
        _deletedAt = Constants.TRANSACTION_UC;
    }
    public TransactionTime(long entryAt)
    {
        assert entryAt <= ToolKit.currentSeconds(): "Cannot create a TransactionTime interval with start time in the future";

        _entryAt = entryAt;
        _deletedAt = Constants.TRANSACTION_UC;
    }

    public TransactionTime(long entryAt, long deletedAt)
    {
        assert entryAt < deletedAt : "Cannot create a TransactionTime interval with start time less than end time";
        assert deletedAt < ToolKit.currentSeconds() : "Cannot create a TransactionTime interval with end time in the future";

        _entryAt = entryAt;
        _deletedAt = deletedAt;
        _deleted = true;
    }

    public long deleteEntry()
    {
        if(!_deleted)
        {
            _deletedAt = ToolKit.currentSeconds();
            _deleted = true;
        }
        return _deletedAt;
    }

    public long entryAt()
    {
        return _entryAt;
    }

    public long deletedAt()
    {
        if(!_deleted)
            return Constants.TRANSACTION_UC;
        else
            return _deletedAt;
    }

    public long currentStretch()
    {
        if(_deleted)
            return _deletedAt;
        else
            return ToolKit.currentSeconds();
    }

    public boolean intersects(TransactionTime other)
    {
        return !(_entryAt > other.currentStretch() || currentStretch() < other.entryAt());
    }

    public boolean Deleted() {
        return _deleted;
    }

    public TransactionTime union(TransactionTime other)
    {
        long l = Math.min(_entryAt, other._entryAt);
        long u = Math.max(currentStretch(), other.currentStretch());
        long curr = ToolKit.currentSeconds();
        boolean isNowRel = !_deleted || !other._deleted;

        if(isNowRel)
            return new TransactionTime(l);
        else
            return new TransactionTime(l, u);
    }

    @Override
    public String toString()
    {
        return "TransactionTime("+entryAt()+" "+deletedAt()+")";
    }
}
