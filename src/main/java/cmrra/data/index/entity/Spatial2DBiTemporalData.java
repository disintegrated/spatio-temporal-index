package cmrra.data.index.entity;

/**
 * Created by rabbiddog on 03.10.17.
 * K: the key type of the data
 */

import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.utility.Log;
import cmrra.data.index.utility.ToolKit;
import com.vividsolutions.jts.geom.*;

import java.io.*;

public abstract class Spatial2DBiTemporalData<K> implements Serializable, ISp2DBiTemporalGeom {

    private int size;
    public final K UUID;

    public Spatial2DBiTemporalData(K UUID, Sp2DBiTemporalBoundary boundary)
    {
        this.UUID = UUID;
        this._boundary = boundary;
        this.size = 0;
    }

    protected Sp2DBiTemporalBoundary _boundary;

    protected boolean _deleted;

    public abstract void delete();

    public boolean isDeleted()
    {
        return _deleted;
    }

    public int size()throws Exception{
        if(size != 0)
            return size;
        else {
            try{
                size = ToolKit.getSize(this);
                return size;
            }catch (Exception ex)
            {
                Log.error(Log.getExceptionString(ex));
                throw ex;
            }
        }
    }

    public boolean resetTransactionTime()
    {
        if(_deleted)
            return false;
        _boundary = _boundary.resetTransactionTime();
        return true;
    }
}
