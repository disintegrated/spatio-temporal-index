package cmrra.data.index.entity;

import cmrra.data.index.utility.ToolKit;

import java.io.Serializable;

/**
 * Created by rabbiddog on 03.10.17.
 * This class does not allow updates
 * To update create a new object
 */

public class ValidTime implements Serializable {

    private long _lowerBoundTime; //in seconds since  UTC 1/1/1970 epoch

    private long _upperBoundTime;//in seconds since  UTC 1/1/1970 epoch

    private boolean _nowRelative;

    private boolean _startedAsNowRelative; //denotes that the shape is a stair case even though the upper bound might be set at some time later

    public ValidTime(long lowerBoundTime)
    {
        long curr = ToolKit.currentSeconds();
        assert lowerBoundTime <= curr : "For VT NOW, the start time should be <= current time";

        this._lowerBoundTime = lowerBoundTime;
        this._nowRelative = true;
        this._upperBoundTime = Constants.VALID_NOW;
        this._startedAsNowRelative = true;
    }

    public ValidTime(long lowerBoundTime, long upperBoundTime)
    {
        assert lowerBoundTime <= upperBoundTime : "For VT fixed, the start time should be <= end time";
        this._lowerBoundTime = lowerBoundTime;
        this._upperBoundTime = upperBoundTime;
        this._nowRelative = false;
        this._startedAsNowRelative = false;
    }

    /**
     * When the validity needs to be fixed after it was NOW relative
     * @param up
     */
    public long setUpperBoundTime(long up)
    {
        assert this._lowerBoundTime <= up : "For VT fixed, the start time should be <= end time";

        if(_nowRelative)
        {
            _nowRelative = false;
            this._upperBoundTime = up;
        }

        return this._upperBoundTime;
        //_startedAsNowRelative remains unchanged
    }

    public long Start()
    {
        return this._lowerBoundTime;
    }

    public boolean startedAsNowRelative(){return _startedAsNowRelative;}

    public long End()
    {
        if(_nowRelative)
            return Constants.VALID_NOW;
        else
            return this._upperBoundTime;
    }

    /**
     * use to find the intersection of axis
     * @return
     */
    public long currentStretch()
    {
        if(_nowRelative)
            return ToolKit.currentSeconds();
        else
            return this._upperBoundTime;
    }

    public boolean isNowRelative()
    {
        return _nowRelative;
    }

    public boolean intersects(ValidTime other)
    {
        return !(_lowerBoundTime > other.currentStretch() || currentStretch() < other.Start());
    }

    public ValidTime union (ValidTime other)
    {
        long l = Math.min(_lowerBoundTime, other._lowerBoundTime);
        long u = Math.max(currentStretch(), other.currentStretch());
        long curr = ToolKit.currentSeconds();
        boolean isNowRel = isNowRelative() || other.isNowRelative();
        if(isNowRel && (u > curr))
            return new ValidTime(l, u);
        else if(isNowRel)
            return new ValidTime(l);
        else
            return new ValidTime(l, u);
    }

    @Override
    public String toString()
    {
        return "ValidTime("+_lowerBoundTime+" "+End()+")";
    }
}
