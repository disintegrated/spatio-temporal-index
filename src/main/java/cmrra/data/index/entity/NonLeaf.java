package cmrra.data.index.entity;

import cmrra.data.index.store.IStore;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rabbiddog on 09.10.17.
 */
public interface NonLeaf<V extends Serializable> extends Node<V> {
    List<Node<V>> children();
    void setChildren(List<Node<V>> c);
    IStore<V> store();
    Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> addNode(int level, Pair<Integer,Node<V>> entry) throws Exception;
}
