package cmrra.data.index.entity;

import java.io.Serializable;

/**
 * Created by rabbiddog on 03.10.17.
 */
public class DoubleInterval implements Serializable {
    private final String TAG = "Class: DoubleInterval ";

    public double start;

    public double end;

    public DoubleInterval(double start, double end)
    {
        assert start <= end: TAG+"start:"+start+ " must be lessthanequal end:"+end;
       // if(start>end)
         //   throw new IllegalArgumentException(TAG+"interval start must be less that equal to the end");

        this.start = start;
        this.end = end;
    }

    public double interval()
    {
        return this.end - this.start;
    }
}
