package cmrra.data.index.entity;

import java.io.Serializable;

/**
 * Created by rabbiddog on 03.10.17.
 */
public class SpatialAxisInterval implements Serializable {

    private final String TAG = "Class: SpatialAxisInterval ";

    public double start;

    public double end;

    public SpatialAxisInterval(double start, double end)
    {
        assert start <= end: TAG+"start:"+start+ " must be lessthanequal end:"+end;

        this.start = start;
        this.end = end;
    }

    public boolean intersects(SpatialAxisInterval other)
    {
        return !(start > other.end || end < other.start);
    }

    public SpatialAxisInterval union(SpatialAxisInterval other)
    {
        double s = Math.min(start, other.start);
        double e = Math.max(end, other.end);

        return new SpatialAxisInterval(s,e);
    }
}
