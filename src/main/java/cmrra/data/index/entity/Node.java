package cmrra.data.index.entity;

import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.utility.Context;
import io.reactivex.ObservableEmitter;
import org.apache.commons.lang3.tuple.*;

import java.io.Serializable;

/**
 * Created by rabbiddog on 03.10.17.
 */

public interface Node<V extends Serializable> extends ISp2DBiTemporalGeom{

    Pair<? extends Node<V>, ? extends Node<V>> add(Spatial2DBiTemporalData<V> data) throws Exception;

    int deleteSoft(Spatial2DBiTemporalData<V> data) throws Exception;

    NodeAndEntries<V> deletePermanent(Spatial2DBiTemporalData<V> data) throws Exception;

    void search(io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion,
                ObservableEmitter<Spatial2DBiTemporalData<V>> emitter);

    void search(io.reactivex.functions.BiFunction<Sp2DBiTemporalBoundary, Class<? extends Object>, Boolean> criterion,
                ObservableEmitter<Spatial2DBiTemporalData<V>> emitter);

    Context context();
}
