package cmrra.data.index.entity;

import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.utility.Context;
import cmrra.data.index.utility.Sp2DBiTNowRelNodeLeafHelper;
import cmrra.data.index.store.IStore;
import cmrra.data.index.utility.Log;
import cmrra.data.index.utility.ToolKit;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.BiFunction;
import org.apache.commons.lang3.tuple.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by rabbiddog on 04.10.17.
 */
public class Sp2DBiTNowRelNodeLeaf<V extends Serializable> extends Sp2DBiTNowRelNode<V> implements Leaf<V>  {

        private HashSet<V> _entries;

        private final IStore<V> _store;

        private final Context _context;

        public Sp2DBiTNowRelNodeLeaf(long id, IStore<V> store, Context context)
        {
            super(id);
            this._store = store;
            _context = context;
            _entries = new HashSet<>();
        }

        public Sp2DBiTNowRelNodeLeaf(long id, HashSet<V> entries, IStore<V> store, Context context) throws Exception
        {
                super(id);
                assert null != entries: "entries in leaf node cannot be assiged a null set";
                this._store = store;
                _context = context;
                _entries = entries;
                if(_entries.size() > 0)
                        updateBoundary();
        }

        @Override
        public Pair<? extends Node<V>, ? extends Node<V>> add(Spatial2DBiTemporalData<V> data) throws Exception
        {
                try
                {
                        Pair<Sp2DBiTNowRelNodeLeaf<V>, Sp2DBiTNowRelNodeLeaf<V>> ret =  Sp2DBiTNowRelNodeLeafHelper.add(data, this);
                        if(null == ret.getRight())
                        {
                                updateBoundary();
                        }
                        else
                        {
                                Sp2DBiTNowRelNode.removeUniqueId(this.nodeId);
                        }

                        return  ret;
                }catch (Exception ex)
                {
                        Log.error(Log.getExceptionString(ex));
                        throw ex;
                }
        }

        @Override
        public int deleteSoft(Spatial2DBiTemporalData<V> data) throws Exception
        {
                try
                {
                        return Sp2DBiTNowRelNodeLeafHelper.deleteSoft(data, this);
                }catch (Exception ex)
                {
                        Log.error(Log.getExceptionString(ex));
                        throw ex;
                }
        }

        @Override
        public NodeAndEntries<V> deletePermanent(Spatial2DBiTemporalData<V> data) throws Exception {
                try
                {
                        NodeAndEntries<V> result = Sp2DBiTNowRelNodeLeafHelper.deletePermanent(data, this);
                        if(result.node().isPresent() && result.countDeleted() > 0)
                                this.updateBoundary(); //if node is present and something was deleted then its the same leaf with one less entry

                        return result;
                }catch (Exception ex)
                {
                        Log.error(Log.getExceptionString(ex));
                        throw ex;
                }
        }

        @Override
        public void search(io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter)
        {
                Sp2DBiTNowRelNodeLeafHelper.search(criterion, emitter, this);
        }

        @Override
        public void search(BiFunction<Sp2DBiTemporalBoundary, Class<? extends Object>, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter) {
                Sp2DBiTNowRelNodeLeafHelper.search(criterion, emitter, this);
        }

        @Override
        public HashSet<V> entries() {
                return _entries;
        }

        @Override
        public IStore<V> store() {
                return _store;
        }

        @Override
        public Context context() {
                return _context;
        }

        @Override
        public SpatialAxisInterval X() {
                return _mbRegion.X();
        }

        @Override
        public SpatialAxisInterval Y() {
                return _mbRegion.Y();
        }

        @Override
        public TransactionTime T() {
                return _mbRegion.T();
        }

        @Override
        public ValidTime V() {
                return _mbRegion.V();
        }

        @Override
        public Sp2DBiTemporalBoundary BB() {
                return _mbRegion;
        }

        @Override
        public boolean hasHidden() {
                return _mbRegion.hasHidden();
        }

        @Override
        protected void updateBoundary() throws Exception
        {
                try{
                        ArrayList<Spatial2DBiTemporalData<V>> dataEntries = new ArrayList<Spatial2DBiTemporalData<V>>(_store.get(_entries).values());
                        this._mbRegion = ToolKit.getMinimumBoundingRegion(dataEntries);
                }catch (Exception ex)
                {
                        Log.error(Log.getExceptionString(ex));
                        throw ex;
                }

        }
}
