package cmrra.data.index.entity;

import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.utility.ToolKit;

import java.util.List;

/**
 * Created by rabbiddog on 10.10.17.
 */
public class Group<T extends ISp2DBiTemporalGeom> {

    private final Sp2DBiTemporalBoundary _mbRegion;
    private final List<T> _entries;

    public Group(List<T> entries)
    {
        this._entries = entries;
        this._mbRegion = ToolKit.getMinimumBoundingRegion(entries);
    }
    public Sp2DBiTemporalBoundary get_mbRegion() {
        return _mbRegion;
    }

    public List<T> get_entries() {
        return _entries;
    }
}
