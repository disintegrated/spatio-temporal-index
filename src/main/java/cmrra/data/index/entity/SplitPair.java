package cmrra.data.index.entity;

import cmrra.data.index.geometry.ISp2DBiTemporalGeom;

import java.util.List;

/**
 * Created by rabbiddog on 10.10.17.
 */
public final class SplitPair<T extends ISp2DBiTemporalGeom> {

    private final Group<T> _group1;
    private final Group<T> _group2;

    private double areaEquiv = -1; //for higher dimension this will be the volume equivalent
    public final double marginSum;

    public SplitPair(List<T> set1, List<T> set2)
    {
        this._group1 = new Group<T>(set1);
        this._group2 = new Group<T>(set2);
        this.marginSum = _group1.get_mbRegion().marginEquivRectangle() + _group2.get_mbRegion().marginEquivRectangle();
    }

    public double areaSum()
    {
        //defer calculation till needed
        if (areaEquiv == -1)
            areaEquiv = _group1.get_mbRegion().areaEquivRectangle() + _group2.get_mbRegion().areaEquivRectangle();
        return areaEquiv;
    }

    public Group<T> group1() {
        return _group1;
    }

    public Group<T> group2() {
        return _group2;
    }
}
