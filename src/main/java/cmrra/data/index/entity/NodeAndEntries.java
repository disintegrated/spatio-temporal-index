package cmrra.data.index.entity;

import com.google.common.base.Optional;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by rabbiddog on 01.11.17.
 * If node() is present then the particular child node on which deletePermananent was called was not deleted.
 * entriesToAdd() gives the entries in the leaf that must be reinserted
 * Q is the list of deleted nodes that need to be reinserted in the tree hierarchy along with the level the node was present
 */
public final class NodeAndEntries<V extends Serializable> {
    private final Optional<? extends Node<V>> _node;
    private HashMap<Integer, HashSet<Node<V>>> _Q;
    private final int _count;
    private int _level;

    public NodeAndEntries(Optional<? extends Node<V>> node, int countDeleted, int level)
    {
        this._node = node;
        //this._entries = entries;
        this._count = countDeleted;
        this._level = level;
        _Q = new HashMap<>();
    }

    public Optional<? extends Node> node() {
        return _node;
    }

    /*public List<T> entriesToAdd() {
        return _entries;
    }*/

    public int countDeleted() {
        return _count;
    }

    public void addToQ(int level, Node N)
    {
        if(_Q.containsKey(level))
        {
            HashSet<Node<V>> s = _Q.get(level);
            s.add(N);
        }
        else
        {
            HashSet<Node<V>> s = new HashSet<>();
            s.add(N);
            _Q.put(level, s);
        }
    }

    public HashMap<Integer, HashSet<Node<V>>> Q()
    {
        return _Q;
    }

    public int  level()
    {
        return _level;
    }
}
