package cmrra.data.index.factories;

import cmrra.data.index.store.MapDBBackedStore;

/**
 * Created by rabbiddog on 07.10.17.
 */
public final class StoreFactory {

    public static MapDBBackedStore createDefaultMapDBBackedStore()
    {
        MapDBBackedStore store = new MapDBBackedStore();

        store.Init("cmrra", false);
        return store;
    }

    public static MapDBBackedStore createNamedMapDBBackedStore(String name)
    {
        MapDBBackedStore store = new MapDBBackedStore();
        store.Init(name, false);
        return store;
    }
}
