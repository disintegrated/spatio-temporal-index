package cmrra.data.index.factories;

import cmrra.data.index.entity.*;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Created by rabbiddog on 06.10.17.
 */
public final class Geometryfactory {


    private static com.vividsolutions.jts.geom.GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();
    /*public static Sp2DBiTemporalBoundary createBoundary(SpatialAxisInterval x, SpatialAxisInterval y)
    {

    }

    public static Sp2DBiTemporalBoundary createBoundary(SpatialAxisInterval x, SpatialAxisInterval y, TransactionTime tt)
    {

    }

    public static Sp2DBiTemporalBoundary createBoundary(SpatialAxisInterval x, SpatialAxisInterval y, ValidTime vt)
    {

    }
    */

    public static ValidTime createValidTime(long start, long end)
    {
        if(end < 0)
            return new ValidTime(start);
        else
            return new ValidTime(start, end);
    }

    public static ValidTime createValidTime(double s, double e)
    {
        long start = (long)s;
        long end = (long)e;
        if(end < 0)
            return new ValidTime(start);
        else
            return new ValidTime(start, end);
    }

    public static TransactionTime createTransactionTime(long start, long end)
    {
        if(end < 0)
            return new TransactionTime(start);
        else
            return new TransactionTime(start, end);
    }

    public static TransactionTime createTransactionTime(double s, double e)
    {
        long start = (long)s;
        long end = (long)e;
        if(end < 0)
            return new TransactionTime(start);
        else
            return new TransactionTime(start, end);
    }


    public static Sp2DBiTemporalBoundary createBoundary(SpatialAxisInterval x, SpatialAxisInterval y, TransactionTime tt, ValidTime vt)
    {
        return new Sp2DBiTemporalBoundary(x, y, tt, vt);
    }

    public static Sp2DBiTemporalBoundary createBoundary(double x_min, double x_max, double y_min, double y_max, long t_min, long t_max, long v_min, long v_max)
    {
        SpatialAxisInterval x = new SpatialAxisInterval(x_min, x_max);
        SpatialAxisInterval y = new SpatialAxisInterval(y_min, y_max);
        TransactionTime t;
        if (t_max ==Constants.TRANSACTION_UC)
             t = new TransactionTime(t_min);
        else
            t = new TransactionTime(t_min, t_max);
        ValidTime v;
        if(v_max ==  Constants.VALID_NOW)
            v = new ValidTime(v_min);
        else
            v = new ValidTime(v_min, v_max);

        return new Sp2DBiTemporalBoundary(x, y, t, v);
    }

    public static Sp2DBiTemporalBoundary createBoundary(double x_min, double x_max, double y_min, double y_max, long t_min, long t_max, long v_min, long v_max, boolean hasHidden)
    {
        SpatialAxisInterval x = new SpatialAxisInterval(x_min, x_max);
        SpatialAxisInterval y = new SpatialAxisInterval(y_min, y_max);
        TransactionTime t;
        if (t_max ==Constants.TRANSACTION_UC)
            t = new TransactionTime(t_min);
        else
            t = new TransactionTime(t_min, t_max);
        ValidTime v;
        if(v_max ==  Constants.VALID_NOW)
            v = new ValidTime(v_min);
        else
            v = new ValidTime(v_min, v_max);

        return new Sp2DBiTemporalBoundary(x, y, t, v, hasHidden);
    }

}
