package cmrra.data.index.factories;

import cmrra.data.index.entity.*;
import cmrra.data.index.utility.Context;
import cmrra.data.index.store.IStore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

/**
 * Created by rabbiddog on 07.10.17.
 */
public final class NodeFactoryDefault implements INodeFactory{

    /**
     * Creates a leaf node that has no parent.
     * Used for creating a Root node
     * @param store the store where to save data
     * @return root node that is of type leaf node
     */
    @Override
    public  <V extends  Serializable> Sp2DBiTNowRelNodeLeaf<V> createLeaf(IStore store, Context context) throws Exception
    {
        long id = Sp2DBiTNowRelNode.getUniqueNodeId();
        HashSet<V> e = new HashSet<>();
        Sp2DBiTNowRelNodeLeaf<V> leaf = new Sp2DBiTNowRelNodeLeaf<V>(id,e, store, context);

        return leaf;
    }


    /**
     * Creates a leaf node that is not a root
     * @param store the store where to save data
     * @return
     */
    @Override
    public <V extends Serializable> Sp2DBiTNowRelNodeLeaf<V> createLeaf(IStore store, HashSet<V> entries, Context context)throws Exception
    {
        long id = Sp2DBiTNowRelNode.getUniqueNodeId();
        Sp2DBiTNowRelNodeLeaf<V> leaf = new Sp2DBiTNowRelNodeLeaf<V>(id, entries, store, context);

        return leaf;
    }

    @Override
    public <V extends Serializable> Sp2DBiTNowRelNodeNonLeaf<V> createNonLeaf(IStore store, List<Node<V>> entries, Context context) throws Exception
    {
        long id = Sp2DBiTNowRelNode.getUniqueNodeId();

        Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf = new Sp2DBiTNowRelNodeNonLeaf<V>(id, store, context, entries);
        return nonLeaf;
    }


}
