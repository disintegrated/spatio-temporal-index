package cmrra.data.index.factories;

import cmrra.data.index.entity.*;
import cmrra.data.index.utility.Context;
import cmrra.data.index.store.IStore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

/**
 * Created by rabbiddog on 09.10.17.
 */
public interface INodeFactory {

    <V extends Serializable> Sp2DBiTNowRelNodeLeaf<V> createLeaf(IStore store, Context context) throws Exception;
    <V extends Serializable> Sp2DBiTNowRelNodeLeaf<V> createLeaf(IStore store, HashSet<V> entries, Context context) throws Exception;
    <V extends Serializable> Sp2DBiTNowRelNodeNonLeaf<V> createNonLeaf(IStore store, List<Node<V>> entries, Context context) throws Exception;
}
