package cmrra.data.index.utility;

/**
 * Created by rabbiddog on 08.10.17.
 */
import cmrra.data.index.entity.*;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.store.IStore;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import com.google.common.base.Optional;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.BiFunction;
import org.apache.commons.lang3.tuple.*;

public class Sp2DBiTNowRelNodeLeafHelper {

    private Sp2DBiTNowRelNodeLeafHelper(){}

    public static <V extends Serializable> Pair<Sp2DBiTNowRelNodeLeaf<V>, Sp2DBiTNowRelNodeLeaf<V>>  add(Spatial2DBiTemporalData<V> entry, Sp2DBiTNowRelNodeLeaf<V> leaf) throws Exception
    {
        try
        {
            SplitPair<Spatial2DBiTemporalData<V>> l_ll ;
            Pair<Sp2DBiTNowRelNodeLeaf<V>, Sp2DBiTNowRelNodeLeaf<V>> finalLeaves;

            Context context = leaf.context();
            /**
            * enter data in the persistent store which is separate from the tree structure
            */
            leaf.store().put(entry);
            /**
             * Set Transaction time entry data. If this data has already been in this store
             * then transaction start time will remain unchanged from previous value
             */
            if(leaf.entries().size() >= leaf.context().maxChildren()) //should never be greater but just to be safe
            {
                /**
                 * split the node
                 */
                HashSet<V> all = ToolKit.add(leaf.entries(), (V)entry.UUID);
                l_ll = context.splitter().splitData(all, context.minChildren(), leaf.store());
                finalLeaves = new ImmutablePair<>(makeLeaf(l_ll.group1(), leaf.store(), leaf.context()), makeLeaf(l_ll.group2(), leaf.store(), leaf.context()));
            }
            else
            {
                /**
                 * Add data identifier to the leaf node
                 * This is akin to adding the data to the leaf
                 */
                leaf.entries().add((V)entry.UUID);
                finalLeaves = new ImmutablePair<Sp2DBiTNowRelNodeLeaf<V>, Sp2DBiTNowRelNodeLeaf<V>>(leaf, null);
            }


            return finalLeaves;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static <V extends Serializable> int deleteSoft(Spatial2DBiTemporalData<V> data, Sp2DBiTNowRelNodeLeaf<V> leaf) throws Exception
    {
        try{
            if(!(leaf.BB().intersectsRectangle(data.BB())))
                return 0;

            int count = 0;
            HashMap<V, Spatial2DBiTemporalData<V>> entries = leaf.store().get(leaf.entries());
            for (Spatial2DBiTemporalData<V> e: entries.values())
            {
                if(data.UUID.equals(e.UUID))
                {
                    leaf.store().remove(e.UUID); //deletes the data part but retains the spatio-temporal boundary
                    count++;
                }
            }

            return count;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static <V extends Serializable> NodeAndEntries<V> deletePermanent(Spatial2DBiTemporalData<V> data, Sp2DBiTNowRelNodeLeaf<V> leaf) throws Exception
    {
        try{
            if(leaf.entries().contains(data.UUID))
            {
                leaf.entries().remove(data.UUID);
                leaf.store().hardDelete(data.UUID);
                if(leaf.entries().size() < leaf.context().minChildren())
                {
                    NodeAndEntries<V> result = new NodeAndEntries<V>(Optional.<Node<V>> absent(),1, 0); //delete this node
                    //result.addToQ(0, leaf); //do this at parent
                    return result;
                }
                else
                {
                    return new NodeAndEntries<V>(Optional.of(leaf), 1, 0);
                }
            }
            else
            {
                return new NodeAndEntries<V>(Optional.of(leaf), 0, 0);
            }
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static <V extends Serializable> void search(io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter, Sp2DBiTNowRelNodeLeaf<V> leaf)
    {
        try{
            if(!criterion.apply(leaf.BB()))
                return;
            for (V eIdx:leaf.entries())
            {
                Spatial2DBiTemporalData<V> e = leaf.store().get(eIdx);
                if(null != e && criterion.apply(e.BB()))
                {
                    if(emitter.isDisposed())
                        return;
                    else
                    {
                        emitter.onNext(e);
                    }
                }
            }
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            if(!emitter.isDisposed())
                emitter.onError(ex);
            return;
        }
    }

    public static <V extends Serializable> void search(BiFunction<Sp2DBiTemporalBoundary, Class<? extends Object>, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter, Sp2DBiTNowRelNodeLeaf<V> leaf)
    {
        try{
            if(!criterion.apply(leaf.BB(), leaf.getClass()))
                return;
            for (V eIdx:leaf.entries())
             {
                 Spatial2DBiTemporalData<V> e = leaf.store().get(eIdx);
                 if(null != e && criterion.apply(e.BB(), e.getClass()))
                 {
                     if(emitter.isDisposed())
                         return;
                     else
                     {
                         emitter.onNext(e);
                     }
                 }
             }
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            if(!emitter.isDisposed())
                emitter.onError(ex);
            return;
        }
    }

    private static <V extends Serializable> Sp2DBiTNowRelNodeLeaf<V> makeLeaf(Group<Spatial2DBiTemporalData<V>> g, IStore<V> store, Context context) throws Exception
    {
        List<V> leafEntries = g.get_entries().stream().map(x-> x.UUID).collect(Collectors.toList());
        HashSet<V> set = new HashSet<>(leafEntries);
        Sp2DBiTNowRelNodeLeaf<V> leaf =  context.nodeFactory().createLeaf(store, set,context);
        return leaf;
    }

}
