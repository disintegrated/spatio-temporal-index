package cmrra.data.index.utility;

/**
 * Created by rabbiddog on 21.10.17.
 */

import cmrra.data.index.entity.Node;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import io.reactivex.*;

import java.io.Serializable;

public class ObservableOnStore<V extends Serializable> implements ObservableOnSubscribe<Spatial2DBiTemporalData<V>>
{
    private final Node<V> _node;
    private final io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> _criterion;
    private final io.reactivex.functions.BiFunction<Sp2DBiTemporalBoundary, Class<? extends Object>, Boolean> _criterion2;

    public ObservableOnStore(Node<V> node, io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion)
    {
        this._node = node;
        this._criterion = criterion;
        this._criterion2 = null;
    }

    public ObservableOnStore(Node<V> node, io.reactivex.functions.BiFunction<Sp2DBiTemporalBoundary,  Class<? extends Object>, Boolean> criterion)
    {
        this._node = node;
        this._criterion = null;
        this._criterion2 = criterion;
    }

    @Override
    public void subscribe(ObservableEmitter<Spatial2DBiTemporalData<V>> e) throws Exception
    {
        try{
            if(null != _criterion)
                 _node.search(_criterion, e);
            else
                _node.search(_criterion2, e);

            e.onComplete();
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            e.onError(ex);
        }
    }
}
