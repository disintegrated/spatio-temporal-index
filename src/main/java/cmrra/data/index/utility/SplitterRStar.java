package cmrra.data.index.utility;

import cmrra.data.index.entity.Node;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.entity.SplitPair;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.store.IStore;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.*;

/**
 * Created by rabbiddog on 09.10.17.
 * Implements the Criteria from R* Tree specification
 * Additional heuristic from GR Tree for Bi temporal data is implemented
 */
public class SplitterRStar implements ISplitter {


    private final Comparator<SplitPair<?  extends ISp2DBiTemporalGeom>> comparator;

    public SplitterRStar() {
        this.comparator = new Comparator<SplitPair<?>>() {

            @Override
            public int compare(SplitPair<?> p1, SplitPair<?> p2) {
                //check overlap first then areaSum
                int value = Double.compare(overlap(p1), overlap(p2));
                if (value == 0) {
                    return Double.compare(p1.areaSum(), p2.areaSum());
                } else {
                    return value;
                }
            }};
    }

    private static Comparator<ISp2DBiTemporalGeom> comparator(SortType sortType) {
        switch (sortType) {
            case X_LOWER:
                return INCREASING_X_LOWER;
            case X_UPPER:
                return INCREASING_X_UPPER;
            case Y_LOWER:
                return INCREASING_Y_LOWER;
            case Y_UPPER:
                return INCREASING_Y_UPPER;
            case T_LOWER:
                return INCREASING_T_LOWER;
            case T_UPPER:
                return INCREASING_T_UPPER;
            case V_LOWER:
                return INCREASING_V_LOWER;
            case V_UPPER:
                return INCREASING_V_UPPER;
            default:
                throw new IllegalArgumentException("unknown SortType " + sortType);
        }
    }



    @Override
    public <V extends Serializable> SplitPair<Spatial2DBiTemporalData<V>> splitData(HashSet<V> items, int minSize, IStore<V> store) throws Exception
    {
        HashMap<V, Spatial2DBiTemporalData<V>> entries = store.<V>get(items);
        if(entries.size() != items.size())
        {
            Set<V> keys = entries.keySet();
            Sets.SetView<V> diff = Sets.difference(items, keys);
            StringBuilder keyStr = new StringBuilder("::");
            for (V diffKey: diff)
            {
                keyStr.append(diffKey);
                keyStr.append("::");
            }
            Log.error("Store inconsistent.Some entries in the Tree don't exist in the DB.");
            throw new MissingResourceException("Store inconsistent.Some entries in the Tree don't exist in the DB.", Spatial2DBiTemporalData.class.getCanonicalName() ,keyStr.toString());
        }

        List<SplitPair<Spatial2DBiTemporalData<V>>> pairs = null;
        double lowestMarginSum = Float.MAX_VALUE;
        List<Spatial2DBiTemporalData<V>> list = null;
        for (SortType sortType : SortType.values()) {
            if (list == null) {
                list = new ArrayList<Spatial2DBiTemporalData<V>>(entries.values());
            }
            Collections.sort(list, comparator(sortType));
            List<SplitPair<Spatial2DBiTemporalData<V>>> p = getPairs(minSize, list);
            double marginSum = marginValueSum(p);
            if (marginSum < lowestMarginSum) {
                lowestMarginSum = marginSum;
                pairs = p;
                // because p uses subViews of list we need to create a new one
                // for further comparisons
                list = null;
            }
        }
        return Collections.min(pairs, comparator);
    }

    @Override
    public <V extends Serializable> SplitPair<Node<V>> splitNode(List<Node<V>> items, int minSize) throws Exception
    {
        List<SplitPair<Node<V>>> pairs = null;
        double lowestMarginSum = Float.MAX_VALUE;
        List<Node<V>> list = null;
        for (SortType sortType : SortType.values()) {
            if (list == null) {
                list = new ArrayList<Node<V>>(items);
            }
            Collections.sort(list, comparator(sortType));
            List<SplitPair<Node<V>>> p = getPairs(minSize, list);
            double marginSum = marginValueSum(p);
            if (marginSum < lowestMarginSum) {
                lowestMarginSum = marginSum;
                pairs = p;
                // because p uses subViews of list we need to create a new one
                // for further comparisons
                list = null;
            }
        }
        return Collections.min(pairs, comparator);
    }

    private enum SortType {
        X_LOWER, X_UPPER, Y_LOWER, Y_UPPER, T_LOWER, T_UPPER, V_LOWER, V_UPPER;
    }

    private static <G extends ISp2DBiTemporalGeom> double marginValueSum(List<SplitPair<G>> list) {
        double sum = 0;
        for (SplitPair<G> p : list)
            sum += p.marginSum;
        return sum;
    }

    @VisibleForTesting
    static <G extends ISp2DBiTemporalGeom> List<SplitPair<G>> getPairs(int minSize, List<G> list) {
        List<SplitPair<G>> pairs = new ArrayList<SplitPair<G>>(list.size() - 2 * minSize + 1); //list.size() is supposedly M+1
        for (int i = minSize; i < list.size() - minSize + 1; i++) {
            // Note that subList returns a view of list so creating list1 and
            // list2 doesn't
            // necessarily incur array allocation costs.
            List<G> list1 = list.subList(0, i); //i exclusive
            List<G> list2 = list.subList(i, list.size());
            SplitPair<G> pair = new SplitPair<G>(list1, list2);
            pairs.add(pair);
        }
        return pairs;
    }

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_X_LOWER = new Comparator<ISp2DBiTemporalGeom>() {

        @Override
        public int compare(ISp2DBiTemporalGeom n1, ISp2DBiTemporalGeom n2) {
            return Double.compare(n1.X().start, n2.X().start);
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_X_UPPER = new Comparator<ISp2DBiTemporalGeom>() {

        @Override
        public int compare(ISp2DBiTemporalGeom n1, ISp2DBiTemporalGeom n2) {
            return Double.compare(n1.X().end, n2.X().end);
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_Y_LOWER = new Comparator<ISp2DBiTemporalGeom>() {

        @Override
        public int compare(ISp2DBiTemporalGeom n1, ISp2DBiTemporalGeom n2) {
            return Double.compare(n1.Y().start, n2.Y().start);
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_Y_UPPER = new Comparator<ISp2DBiTemporalGeom>() {

        @Override
        public int compare(ISp2DBiTemporalGeom n1, ISp2DBiTemporalGeom n2) {
            return Double.compare(n1.Y().end, n2.Y().end);
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_T_LOWER = new Comparator<ISp2DBiTemporalGeom>() {
        @Override
        public int compare(ISp2DBiTemporalGeom o1, ISp2DBiTemporalGeom o2) {
            return Double.compare(o1.T().entryAt(), o2.T().entryAt());
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_T_UPPER = new Comparator<ISp2DBiTemporalGeom>() {
        @Override
        public int compare(ISp2DBiTemporalGeom o1, ISp2DBiTemporalGeom o2) {
            return Double.compare(o1.T().currentStretch(), o2.T().currentStretch());
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_V_LOWER = new Comparator<ISp2DBiTemporalGeom>() {
        @Override
        public int compare(ISp2DBiTemporalGeom o1, ISp2DBiTemporalGeom o2) {
            return Double.compare(o1.V().Start(), o2.V().Start());
        }
    };

    private static final Comparator<ISp2DBiTemporalGeom> INCREASING_V_UPPER = new Comparator<ISp2DBiTemporalGeom>() {
        @Override
        public int compare(ISp2DBiTemporalGeom o1, ISp2DBiTemporalGeom o2) {
            return Double.compare(o1.V().currentStretch(), o1.V().currentStretch());
        }
    };

    private static double overlap(SplitPair<? extends ISp2DBiTemporalGeom> pair)
    {
        return pair.group1().get_mbRegion().areaEquivIntersectRectangle(pair.group2().get_mbRegion());
    }
}
