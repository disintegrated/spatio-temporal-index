package cmrra.data.index.utility;

import cmrra.data.index.entity.Node;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.*;
import cmrra.data.index.utility.ToolKit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by rabbiddog on 17.10.17.
 */
public class Comparators {


    private Comparators() {}

    public static <V extends Serializable> Comparator<ISp2DBiTemporalGeom>  overlapAreaThenAreaIncreaseThenAreaComparator(
            ISp2DBiTemporalGeom  r, final List<? extends Node<V>> list) {

        return new Comparator<ISp2DBiTemporalGeom>() {

            @Override
            public int compare(ISp2DBiTemporalGeom g1, ISp2DBiTemporalGeom g2) {
                int value = Float.compare(overlapArea(r, list, g1), overlapArea(r, list, g2));
                if (value == 0) {
                    value = Double.compare(areaIncrease(r, g1), areaIncrease(r, g2));
                    if (value == 0) {
                        value = Double.compare(area(r, g1), area(r, g2));
                    }
                }
                return value;
            }
        };
    }

    private static double area(final ISp2DBiTemporalGeom r, ISp2DBiTemporalGeom g1)
    {
        List<ISp2DBiTemporalGeom> l = new ArrayList<>();
        l.add(g1);
        l.add(r);
        Sp2DBiTemporalBoundary bb = ToolKit.getMinimumBoundingRegion(l);

        return bb.areaEquivRectangle();
    }

    public static Comparator<ISp2DBiTemporalGeom> areaIncreaseThenAreaComparator(
            final ISp2DBiTemporalGeom r) {
        return new Comparator<ISp2DBiTemporalGeom>() {
            @Override
            public int compare(ISp2DBiTemporalGeom g1, ISp2DBiTemporalGeom g2) {
                int value = Double.compare(areaIncrease(r, g1), areaIncrease(r, g2));
                if (value == 0) {
                    value = Double.compare(area(r, g1), area(r, g2));
                }
                return value;
            }
        };
    }

    private static float overlapArea(ISp2DBiTemporalGeom r, List<? extends ISp2DBiTemporalGeom> list,
                                     ISp2DBiTemporalGeom g) {
        List<ISp2DBiTemporalGeom> l = new ArrayList<>();
        l.add(g);
        l.add(r);
        Sp2DBiTemporalBoundary gPlusR = ToolKit.getMinimumBoundingRegion(l);
        float m = 0;
        for (ISp2DBiTemporalGeom other : list) {
            if (other != g) {
                m += gPlusR.areaEquivIntersectRectangle(other.BB());
            }
        }
        return m;
    }

    private static double areaIncrease(ISp2DBiTemporalGeom r, ISp2DBiTemporalGeom g)
    {
        List<ISp2DBiTemporalGeom> l = new ArrayList<>();
        l.add(g);
        l.add(r);
        Sp2DBiTemporalBoundary bb = ToolKit.getMinimumBoundingRegion(l);
        return bb.areaEquivRectangle() - g.BB().areaEquivRectangle();
    }

    /**
     * <p>
     * Returns a comparator that can be used to sort entries returned by search
     * methods. For example:
     * </p>
     * <p>
     * <code>search(100).toSortedList(ascendingDistance(r))</code>
     * </p>
     *
     * @param r
     *            Sp2DBiTemporalBoundary to measure distance to
     * @return a comparator to sort by ascending distance from the Sp2DBiTemporalBoundary
     */
    /*public static <G extends ISp2DBiTemporalGeom> Comparator<G> ascendingDistance(
            final G r) {
        return new Comparator<Entry<T, S>>() {
            @Override
            public int compare(Entry<T, S> e1, Entry<T, S> e2) {
                return Double.compare(e1.geometry().distance(r), e2.geometry().distance(r));
            }
        };
    }*/
}
