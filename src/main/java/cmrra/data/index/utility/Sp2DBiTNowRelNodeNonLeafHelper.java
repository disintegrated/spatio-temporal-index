package cmrra.data.index.utility;

import cmrra.data.index.entity.*;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.store.IStore;

import java.io.Serializable;
import java.util.*;

import com.google.common.base.*;
import com.google.common.base.Optional;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.BiFunction;
import org.apache.commons.lang3.tuple.*;

/**
 * Created by rabbiddog on 08.10.17.
 */
public class Sp2DBiTNowRelNodeNonLeafHelper {

    private Sp2DBiTNowRelNodeNonLeafHelper(){}

    public static <V extends Serializable> Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> add(Spatial2DBiTemporalData<V> entry, Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf) throws Exception
    {
        try{
            Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> finalNodes;
            List<Node<V>> children = nonLeaf.children();
            Context context = nonLeaf.context();
            Node<V> child =  context.selector().select(entry, children);
            Pair<? extends Node<V>, ? extends Node<V>> child2 =   child.add(entry);
            if(null != child2.getRight())
            {
               /* nonLeaf.children().remove(child);
                nonLeaf.children().add(child2.getLeft());
                nonLeaf.children().add(child2.getRight());*/

                nonLeaf.setChildren(ToolKit.replace(children, child2, child));

                if(nonLeaf.children().size() > context.maxChildren())
                {
                    SplitPair<Node<V>> splitPair = context.splitter().splitNode(nonLeaf.children(), context.minChildren());
                    finalNodes = new ImmutablePair<>(makeNonLeaf(splitPair.group1(), nonLeaf.store(), nonLeaf.context()), makeNonLeaf(splitPair.group2(), nonLeaf.store(), nonLeaf.context()));
                }
                else
                {
                    finalNodes = new ImmutablePair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>>(nonLeaf, null);
                }
            }
            else
            {
                finalNodes = new ImmutablePair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>>(nonLeaf, null);
            }

            return finalNodes;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static <V extends Serializable> NodeAndEntries<V> deletePermanent(Spatial2DBiTemporalData<V> data, Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf) throws Exception
    {
        List<Node<V>> removeTheseNodes = new ArrayList<Node<V>>();
        HashMap<Integer, HashSet<Node<V>>> _Q = new HashMap<>();
        int countDeleted = 0;
        int level = 0;
        for (Node<V> child: nonLeaf.children())
        {
            if(child.BB().intersectsRectangle(data.BB()))
            {
                NodeAndEntries<V> result = child.deletePermanent(data);
                if(result.countDeleted() == 0)
                    continue;
                countDeleted += result.countDeleted();
                if(!result.node().isPresent())
                {
                    //nonLeaf.children().remove(child);
                    level = result.level() +1;
                    //add all entries in its Q to _Q here
                    for (Map.Entry<Integer, HashSet<Node<V>>> e:result.Q().entrySet())
                    {
                        if(_Q.containsKey(e.getKey()))
                        {
                            HashSet<Node<V>> set = _Q.get(e.getKey());
                            for (Node<V> n: e.getValue())
                            {
                                set.add(n);
                            }
                        }
                    }
                    removeTheseNodes.add(child);
                    HashSet<Node<V>> ch;
                    if(_Q.containsKey(result.level()))
                    {
                        ch = _Q.get(result.level());
                        ch.add(child);
                    }
                    else
                    {
                        ch = new HashSet<>();
                        ch.add(child);
                        _Q.put(result.level(), ch);
                    }
                }
            }
        }

        for (Node<V> n: removeTheseNodes)
        {
            nonLeaf.children().remove(n);
        }
        if(nonLeaf.children().size() < nonLeaf.context().minChildren())
        {
            NodeAndEntries<V> result = new NodeAndEntries<V>(Optional.<Node<V>> absent(), countDeleted, level);
            result.addToQ(level, nonLeaf);
            return result;
        }
        else
        {
            NodeAndEntries<V> result = new NodeAndEntries<V>(Optional.of(nonLeaf), countDeleted, level);
            return result;
        }
    }

    public static <V extends Serializable> int deleteSoft(Spatial2DBiTemporalData<V> data, Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf) throws Exception
    {
        try
        {
            if(!(nonLeaf.BB().intersectsRectangle(data.BB())))
                return 0;
            int count = 0;

            for (Node<V> child : nonLeaf.children())
            {
                if(child.BB().intersectsRectangle(data.BB()))
                {
                    count += child.deleteSoft(data);
                }
            }

            return count;
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    public static <V extends Serializable> void search(io.reactivex.functions.Function<Sp2DBiTemporalBoundary, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter, Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf)
    {
        try
        {
            if(!criterion.apply(nonLeaf.BB()))
                return;
            for (Node<V> c : nonLeaf.children())
            {
                if(criterion.apply(c.BB()))
                {
                    if(emitter.isDisposed())
                        return;
                    else
                        c.search(criterion, emitter);
                }
            }

        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            if(!emitter.isDisposed())
                emitter.onError(ex);
            return;
        }
    }

    public static <V extends Serializable> void search(BiFunction<Sp2DBiTemporalBoundary, Class<?>, Boolean> criterion, ObservableEmitter<Spatial2DBiTemporalData<V>> emitter, Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf)
    {
        try
        {
            if(!criterion.apply(nonLeaf.BB(), nonLeaf.getClass()))
                return;
            for (Node<V> c : nonLeaf.children())
            {
                if(criterion.apply(c.BB(), c.getClass()))
                {
                    if(emitter.isDisposed())
                        return;
                    else
                        c.search(criterion, emitter);
                }
            }

        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            if(!emitter.isDisposed())
                emitter.onError(ex);
            return;
        }
    }

    public static <V extends Serializable> Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> addNode(int level, Pair<Integer, Node<V>> entry, Sp2DBiTNowRelNodeNonLeaf<V> nonLeaf) throws Exception
    {
        try
        {
            Context context = nonLeaf.context();
            if(level == (entry.getLeft()+1))
            {
                Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> finalNodes;
                //add as a child and call splitter if crossed max children
                nonLeaf.children().add(entry.getRight());

                if(nonLeaf.children().size() > context.maxChildren())
                {
                    SplitPair<Node<V>> splitPair = context.splitter().splitNode(nonLeaf.children(), context.minChildren());
                    finalNodes = new ImmutablePair<>(makeNonLeaf(splitPair.group1(), nonLeaf.store(), nonLeaf.context()), makeNonLeaf(splitPair.group2(), nonLeaf.store(), nonLeaf.context()));
                }
                else
                {
                    finalNodes = new ImmutablePair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>>(nonLeaf, null);
                }
                return finalNodes;
            }
            else if(level == (entry.getLeft()+2))
            {
                Pair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>> finalNodes;
                //call selector to find the best match
                Node<V> child =  nonLeaf.context().selector().select(entry.getRight(), nonLeaf.children());
                if(Leaf.class.isAssignableFrom(child.getClass()))
                {
                    throw new Exception("Reached level:"+level+", which lower than possible insertion point. Cannot insert node at leaf");
                }
                NonLeaf<V> cast = (NonLeaf<V>)child;
                Pair<? extends Node<V>, ? extends Node<V>> child2 =   cast.addNode(level-1, entry);

                if(null != child2.getRight())
                {
                    nonLeaf.children().remove(child);
                    nonLeaf.children().add(child2.getLeft());
                    nonLeaf.children().add(child2.getRight());

                    if(nonLeaf.children().size() > context.maxChildren())
                    {
                        SplitPair<Node<V>> splitPair = context.splitter().splitNode(nonLeaf.children(), context.minChildren());
                        finalNodes = new ImmutablePair<>(makeNonLeaf(splitPair.group1(), nonLeaf.store(), nonLeaf.context()), makeNonLeaf(splitPair.group2(), nonLeaf.store(), nonLeaf.context()));
                    }
                    else
                    {
                        finalNodes = new ImmutablePair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>>(nonLeaf, null);
                    }
                }
                else
                {
                    finalNodes = new ImmutablePair<Sp2DBiTNowRelNodeNonLeaf<V>, Sp2DBiTNowRelNodeNonLeaf<V>>(nonLeaf, null);
                }

                return finalNodes;
            }
            else if(level == 1) //should not reach leaf
            {
                throw new Exception("Reached level:"+level+", which lower than possible insertion point. Cannot insert node at leaf");
            }
            else
            {
                throw new Exception("Reached level:"+level+", which lower than the insertion point in addNode : "+entry.getLeft());
            }
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
    }

    private static <V extends Serializable> Sp2DBiTNowRelNodeNonLeaf<V> makeNonLeaf(Group<Node<V>> g,IStore<V> store, Context context) throws Exception
    {
        return context.nodeFactory().createNonLeaf(store, g.get_entries(), context);
    }
}
