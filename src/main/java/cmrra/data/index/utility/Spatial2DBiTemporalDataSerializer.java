package cmrra.data.index.utility;

/**
 * Created by rabbiddog on 06.10.17.
 */
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.entity.Spatial2DBiTemporalNowRelativeBinaryData;
import org.jetbrains.annotations.NotNull;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;

import java.io.*;

public class Spatial2DBiTemporalDataSerializer {

    public <T extends Spatial2DBiTemporalData<V> & Serializable, V extends Serializable>  byte[] serialize(@NotNull T value) throws Exception
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {

            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(value);
            out.flush();
            byte[] byteArray = bos.toByteArray();

            return byteArray;
        }catch (IOException ioex)
        {
            Log.error(Log.getExceptionString(ioex));
            throw ioex;
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
        finally {
            bos.close();
        }
    }

    public <V extends Serializable> Spatial2DBiTemporalData<V> deserialize(byte[] data) throws Exception
    {
        if(null == data)
            return null;
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        ObjectInput in = null;
        try{
            in = new ObjectInputStream(bis);
            Object o = in.readObject();
            if(Spatial2DBiTemporalData.class.isAssignableFrom(o.getClass()))
                return (Spatial2DBiTemporalData<V>)o; //will fail for incorrect type
            else
                throw new ClassCastException("object "+o.getClass()+" cannot be cast into type Spatial2DBiTemporalData");
        }catch (IOException ioex)
        {
            Log.error(Log.getExceptionString(ioex));
            throw ioex;
        }catch (ClassNotFoundException clex)
        {
            Log.error(Log.getExceptionString(clex));
            throw new Exception("Class not found exception was generated");
        }
        catch (Exception ex)
        {
            Log.error(Log.getExceptionString(ex));
            throw ex;
        }
        finally {
            bis.close();
            if(null != in)
                in.close();
        }
    }
}
