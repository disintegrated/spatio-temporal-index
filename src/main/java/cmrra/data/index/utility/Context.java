package cmrra.data.index.utility;

import cmrra.data.index.factories.INodeFactory;
import cmrra.data.index.store.IStore;
import com.google.common.base.*;

/**
 * Created by rabbiddog on 08.10.17.
 */
public final class Context {

    private final int _maxChildren;
    private final int _minChildren;
    private final ISplitter _splitter;
    private final INodeFactory _nodeFactory; //
    private final ISelector _selector;
    private final IStore _store;
    private final Configuration _configuration;

    public Context(int minChildren, int maxChildren, ISplitter splitter, INodeFactory nodeFactory, ISelector selector, IStore store, Configuration configuration)
    {
        assert maxChildren >= 4 : "Maximum number of children for the Tree must be greater than 2";
        assert minChildren >=2 : "Minimum number of children for the Tree must be greaterThanEqual 1";
        assert maxChildren>= (minChildren/2) : "Maximum number of children must be greater than Minimum number of children";
        assert splitter != null : "ISplitter value needs to be defined";
        assert nodeFactory != null: "Nodefactory needs to be defined";
        assert selector != null: "Selector needs to be defined";
        assert store != null: "Store needs to be provided";
        assert configuration != null : "Configuration object must be set";
        assert !Strings.isNullOrEmpty(configuration.getDBName()) : "Node not set in Configuration object";

        this._minChildren = minChildren;
        this._maxChildren = maxChildren;
        this._splitter = splitter;
        this._nodeFactory = nodeFactory;
        this._selector = selector;
        this._store = store;
        this._configuration = configuration;
    }
    public int maxChildren() {
        return _maxChildren;
    }

    public int minChildren() {
        return _minChildren;
    }

    public ISplitter splitter(){return _splitter;}

    public INodeFactory nodeFactory(){return _nodeFactory;}

    public ISelector selector(){return _selector;}

    public IStore store(){return _store;}
}
