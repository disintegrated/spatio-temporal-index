package cmrra.data.index.utility;

import cmrra.data.index.entity.Node;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;

import static java.util.Collections.min;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rabbiddog on 17.10.17.
 */
public class SelectorMinimumAreaIncrease implements ISelector {

    @Override
    public <V extends Serializable, N extends Node<V>> N select(ISp2DBiTemporalGeom geom, List<N> nodes) {
        return min(nodes, Comparators.areaIncreaseThenAreaComparator(geom));
    }
}
