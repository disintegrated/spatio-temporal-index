package cmrra.data.index.utility;

import cmrra.data.index.entity.Node;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;

import java.io.Serializable;
import java.util.List;

import static java.util.Collections.min;

/**
 * Created by rabbiddog on 17.10.17.
 */
public class SelectorMinimumAreaOverlap implements ISelector {

    @Override
    public <V extends Serializable, N extends Node<V>> N select(ISp2DBiTemporalGeom geom, List<N> nodes) {
        return min(nodes, Comparators.overlapAreaThenAreaIncreaseThenAreaComparator(geom, nodes));
    }
}
