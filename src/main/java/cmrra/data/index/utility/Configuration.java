package cmrra.data.index.utility;

import java.util.UUID;

/**
 * Created by rabbiddog on 03.10.17.
 */
public class Configuration {

    private String _db;

    public Configuration()
    {
    }

    /*Network wide unique ID for this Node*/
    public void setDBName(String nd)
    {
        this._db = nd;
    }

    public String getDBName()
    {
        return _db;
    }


}
