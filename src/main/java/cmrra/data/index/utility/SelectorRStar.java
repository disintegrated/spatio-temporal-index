package cmrra.data.index.utility;

import cmrra.data.index.entity.*;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rabbiddog on 17.10.17.
 */
public class SelectorRStar implements ISelector {
    private static ISelector overlapAreaSelector = new SelectorMinimumAreaOverlap();
    private static ISelector areaIncreaseSelector = new SelectorMinimumAreaIncrease();

    @Override
    public <V extends Serializable, N extends Node<V>> N select(ISp2DBiTemporalGeom geom, List<N> nodes) {
        boolean leafNodes = nodes.get(0) instanceof Leaf;
        if (leafNodes)
            return overlapAreaSelector.select(geom, nodes);
        else
            return areaIncreaseSelector.select(geom, nodes);
    }
}
