package cmrra.data.index.utility;

import cmrra.data.index.entity.*;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.store.IStore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

/**
 * Created by rabbiddog on 06.10.17.
 */
public interface ISplitter {

    /**
     * Splits a list of items into two lists of at least minSize.
     * T: must have Sp2BiTemporal geometry and must be data type of Spatial2DBiTemporalData
     * V: key type of the store
     * The 2 lists returned are the
     * @param items
     * @param minSize
     * @return
     */
    <V extends Serializable> SplitPair<Spatial2DBiTemporalData<V>> splitData(HashSet<V> items, int minSize, IStore<V> store) throws Exception;

    <V extends Serializable> SplitPair<Node<V>> splitNode(List<Node<V>> items, int minSize) throws Exception;

}
