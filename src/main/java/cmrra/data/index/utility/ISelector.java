package cmrra.data.index.utility;

import cmrra.data.index.entity.Node;
import cmrra.data.index.entity.Spatial2DBiTemporalData;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rabbiddog on 17.10.17.
 * Selects a child node based on a criteria.
 * Criteria is defiined in specific implementations
 */
public interface ISelector {

    <V extends Serializable, N extends Node<V>> N select(ISp2DBiTemporalGeom geom, List<N> nodes);
}
