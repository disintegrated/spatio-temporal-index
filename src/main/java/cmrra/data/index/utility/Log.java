package cmrra.data.index.utility;

/**
 * Created by rabbiddog on 04.10.17.
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Log {
    private static Logger _log;
    static {
        _log = LogManager.getRootLogger();
    }

    public static void debug(String message)
    {
        _log.debug(message);
    }

    public static void info(String message)
    {
        _log.info(message);
    }

    public static void error(String message)
    {
        _log.error(message);
    }

    public static void warn(String message)
    {
        _log.warn(message);
    }

    public static String getExceptionString(Exception ex)
    {
        return "Message: "+ex.getMessage()+". StackTrace: "+ex.getStackTrace() ;
    }

    public static String getExceptionString(Throwable ex)
    {
        return "Message: "+ex.getMessage()+". StackTrace: "+ex.getStackTrace() ;
    }

    public static String getExceptionMessage(Exception ex)
    {
        return "Message: "+ex.getMessage();
    }
}
