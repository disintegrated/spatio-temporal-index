package cmrra.data.index.utility;

import cmrra.data.index.factories.Geometryfactory;
import cmrra.data.index.geometry.ISp2DBiTemporalGeom;
import cmrra.data.index.geometry.Sp2DBiTemporalBoundary;
import cmrra.data.index.entity.*;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

/**
 * Created by rabbiddog on 03.10.17.
 */
public class ToolKit {

    public static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();
    private static  Configuration configuration;
    private static Spatial2DBiTemporalDataSerializer serializer = new Spatial2DBiTemporalDataSerializer();
    public static Function<Boolean, Long> external_timeCalculator = null;

    public static void init (Configuration c)
    {
        configuration = c;
    }
    /*generates a UUID for the data generated at this node*/
    public static String getUUID()
    {
        UUID id = UUID.randomUUID();
        return configuration.getDBName() +id.toString();
    }

    public static <V> HashSet<V> add(HashSet<V> entries, V element)
    {
        HashSet<V>  result = new HashSet<>();
        result.addAll(entries);
        result.add(element);
        return result;
    }

    public static <V extends Serializable> List<Node<V>> replace(List<Node<V>> l, Pair<? extends Node<V>, ? extends Node<V>> replacements, Node<V> element)
    {
        List<Node<V>> newList = new ArrayList<>();
        for(Node<V> node: l)
        {
            if(node != element)
                newList.add(node);
        }
        newList.add(replacements.getLeft());
        newList.add(replacements.getRight());
        return newList;
    }

    public static long currentSeconds()
    {
        if(null == external_timeCalculator)
        {
            long milli = System.currentTimeMillis();
            return milli/1000;
        }
        else
        {
            return external_timeCalculator.apply(true);
        }
    }

    public static <T extends ISp2DBiTemporalGeom> Sp2DBiTemporalBoundary getMinimumBoundingRegion(List<T> entries)
    {
        assert null != entries : "getMinimumBoundingRegion received null list";
        assert entries.size() > 0 : "getMinimumBoundingRegion received empty list";
        double x_min = Double.MAX_VALUE;
        double x_max = -Double.MAX_VALUE;
        double y_min = Double.MAX_VALUE;
        double y_max = -Double.MAX_VALUE;
        long t_min = Long.MAX_VALUE;
        long t_max = 0;
        long v_min = Long.MAX_VALUE;
        long v_max = 0;

        boolean isHidden = false;

        try{
            for (T entry:entries)
            {
                if(entry.X().start < x_min)
                    x_min = entry.X().start;
                if(entry.X().end > x_max)
                    x_max = entry.X().end;
                if(entry.Y().start < y_min)
                    y_min = entry.Y().start;
                if(entry.Y().end > y_max)
                    y_max = entry.Y().end;
                if(entry.T().entryAt() < t_min)
                    t_min = entry.T().entryAt();
                if(t_max != Constants.TRANSACTION_UC)
                {
                    if(!entry.T().Deleted())
                        t_max = Constants.TRANSACTION_UC;
                    else if(entry.T().deletedAt() > t_max)
                        t_max = entry.T().deletedAt();
                }
                if(entry.V().Start() < v_min)
                    v_min = entry.V().Start();
                if(v_max != Constants.VALID_NOW)
                {
                    if(entry.V().isNowRelative())
                    {
                        if(v_max <= ToolKit.currentSeconds())
                            v_max = Constants.VALID_NOW;
                        else
                            isHidden = true;
                    }
                    else if(entry.V().End() > v_max)
                        v_max = entry.V().End();
                }
                else if(!entry.V().isNowRelative() && entry.V().End() > ToolKit.currentSeconds())
                {
                    v_max = entry.V().End();
                    isHidden = true;
                }

                isHidden = isHidden || entry.hasHidden();
            }
            Sp2DBiTemporalBoundary bb = Geometryfactory.createBoundary(x_min, x_max, y_min, y_max, t_min, t_max, v_min, v_max, isHidden);

            return bb;
        }catch (Exception ex)
        {
            Log.error(Log.getExceptionMessage(ex));
            throw ex;
        }
    }

    public static int getSize(Spatial2DBiTemporalData data)  throws Exception
    {
        try{
            byte[] b = serializer.serialize(data);
            return b.length;
        }catch (IOException ioex)
        {
            Log.error(Log.getExceptionString(ioex));
            throw ioex;
        }
    }
}
