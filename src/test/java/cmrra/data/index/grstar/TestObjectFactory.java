package cmrra.data.index.grstar;

import cmrra.data.index.entity.Spatial2DBiTemporalNowRelativeBinaryData;
import cmrra.data.index.utility.ToolKit;
import com.vividsolutions.jts.geom.*;

import java.util.HashMap;

/**
 * Created by rabbiddog on 20.10.17.
 */
public class TestObjectFactory {

    private static com.vividsolutions.jts.geom.GeometryFactory GEOMETRY_FACTORY = new GeometryFactory();

    public static Polygon createRandomGeometry(Class<Polygon> cls)
    {
        CoordinateList l = new CoordinateList();
        l.add(new Coordinate(0,0));
        l.add(new Coordinate(40, 0));
        l.add(new Coordinate(40,40));
        l.add(new Coordinate(0, 0));

        return GEOMETRY_FACTORY.createPolygon(l.toCoordinateArray());
    }

    public static Point p1()
    {
        return GEOMETRY_FACTORY.createPoint(new Coordinate(80, 80));
    }

    public static Point p2()
    {
        return GEOMETRY_FACTORY.createPoint(new Coordinate(220.37, 180));
    }

    public static Point p3()
    {
        return GEOMETRY_FACTORY.createPoint(new Coordinate(400, 820));
    }

    public static Point p4()
    {
        return GEOMETRY_FACTORY.createPoint(new Coordinate(240, 535));
    }

    public static byte[] byteData()
    {
        return "O2: 23%".getBytes();
    }

    public static HashMap<String, Spatial2DBiTemporalNowRelativeBinaryData> dataSet()
    {

        HashMap<String, Spatial2DBiTemporalNowRelativeBinaryData> dataCollection = new HashMap<>();

        long now = ToolKit.currentSeconds();
        CoordinateList l1 = new CoordinateList();
        l1.add(new Coordinate(0,0));
        l1.add(new Coordinate(40, 0));
        l1.add(new Coordinate(40,40));
        l1.add(new Coordinate(0, 0));

        Spatial2DBiTemporalNowRelativeBinaryData data1 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(GEOMETRY_FACTORY.createPolygon(l1.toCoordinateArray()), now-400000, true, byteData());
        dataCollection.put(data1.UUID, data1);

        Spatial2DBiTemporalNowRelativeBinaryData data2 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(GEOMETRY_FACTORY.createPolygon(l1.toCoordinateArray()), now-200000, now-190000, true, byteData());
        dataCollection.put(data2.UUID, data2);

        Spatial2DBiTemporalNowRelativeBinaryData data3 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p1(), now-400000, now-180000, false, byteData());
        dataCollection.put(data3.UUID, data3);

        Spatial2DBiTemporalNowRelativeBinaryData data4 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p1(), now-400000, now-180000, false, byteData());
        dataCollection.put(data4.UUID, data4);

        Spatial2DBiTemporalNowRelativeBinaryData data5 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p2(), now-600000, now-210000, false, byteData());
        dataCollection.put(data5.UUID, data5);

        Spatial2DBiTemporalNowRelativeBinaryData data6 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p2(), now-20, false, byteData());
        dataCollection.put(data6.UUID, data6);

        Spatial2DBiTemporalNowRelativeBinaryData data7 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p3(), now-410000, false, byteData());
        dataCollection.put(data7.UUID, data7);

        Spatial2DBiTemporalNowRelativeBinaryData data8 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p3(), now-20, false, byteData());
        dataCollection.put(data8.UUID, data8);

        Spatial2DBiTemporalNowRelativeBinaryData data9 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p4(), now-350000, true, byteData());
        dataCollection.put(data9.UUID, data9);

        Spatial2DBiTemporalNowRelativeBinaryData data10 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p4(), now-250000, true, byteData());
        dataCollection.put(data10.UUID, data10);

        Spatial2DBiTemporalNowRelativeBinaryData data11 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(p4(), now-150000, true, byteData());
        dataCollection.put(data11.UUID, data11);

        return dataCollection;
    }
}
