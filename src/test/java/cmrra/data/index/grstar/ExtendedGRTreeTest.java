package cmrra.data.index.grstar;

/**
 * Created by rabbiddog on 19.10.17.
 */
import cmrra.data.index.factories.Geometryfactory;
import cmrra.data.index.store.MapDBBackedStore;
import cmrra.data.index.utility.Log;
import com.google.common.base.Optional;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import org.junit.Assert;
import org.junit.Test;
import cmrra.data.index.entity.*;
import cmrra.data.index.utility.ToolKit;
import cmrra.data.index.utility.Configuration;

import java.util.HashMap;

public class ExtendedGRTreeTest {

    int count;

    @Test
    public void testInstantiation()
    {
        ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

        Assert.assertNotNull(rtree);
    }

    @Test
    public void testAdd()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            HashMap<String, Spatial2DBiTemporalNowRelativeBinaryData> data = TestObjectFactory.dataSet();
            for (Spatial2DBiTemporalNowRelativeBinaryData d : data.values())
            {
                rtree.add(d);
            }

            Assert.assertNotNull(rtree);
        }
        catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testAddDeleted()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            HashMap<String, Spatial2DBiTemporalNowRelativeBinaryData> data = TestObjectFactory.dataSet();
            for (Spatial2DBiTemporalNowRelativeBinaryData d : data.values())
            {
                d.delete();
                rtree.add(d);
            }

            Assert.fail();
        }
        catch (Exception ex)
        {
            Assert.assertTrue(IllegalArgumentException.class.isAssignableFrom(ex.getClass()));
        }
    }


    @Test
    public void testSearch()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            populateTree(rtree);

            Assert.assertNotNull(rtree);
            long now = ToolKit.currentSeconds();
            Observable<Spatial2DBiTemporalData<String>> observable = rtree.search(Geometryfactory.createBoundary(0, Long.MAX_VALUE, 0, Long.MAX_VALUE, 0, Constants.TRANSACTION_UC, now-500, now));
            count = 0;

            observable.subscribe(new DisposableObserver<Spatial2DBiTemporalData<String>>() {
                @Override
                public void onNext(Spatial2DBiTemporalData<String> data) {
                    System.out.println(data.UUID);
                    System.out.println(data.getClass());

                    count++;
                }

                @Override
                public void onError(Throwable e) {
                    System.out.print(e.getLocalizedMessage());
                }

                @Override
                public void onComplete() {
                    System.out.print("Done");
                }
            });

            Assert.assertTrue(count> 0);
        }
        catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testSearchForAfterTransactionTime()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            populateTree(rtree);
            Assert.assertNotNull(rtree);
            Thread.sleep(10000);
            long now = ToolKit.currentSeconds();
            Spatial2DBiTemporalNowRelativeBinaryData data3 = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(TestObjectFactory.p1(), now-400000, now-180000, false, TestObjectFactory.byteData());
            rtree.add(data3);

            Observable<Spatial2DBiTemporalData<String>> observable = rtree.searchWithOperator(new Operation(OPERATOR.GREATER_THAN, FIELD.T_START, now-5));
            count = 0;

            DisposableObserver<Spatial2DBiTemporalData<String>> observer = new DisposableObserver<Spatial2DBiTemporalData<String>>() {
                @Override
                public void onNext(Spatial2DBiTemporalData<String> data) {
                    System.out.print(data.UUID);
                    System.out.println(data.getClass());
                    count++;
                }

                @Override
                public void onError(Throwable e) {
                    System.out.print(e.getLocalizedMessage());
                }

                @Override
                public void onComplete() {
                    System.out.print("Done");
                }
            };
            observable.subscribe(observer);

            observer.dispose();

            Assert.assertTrue(count>= 1);
        }
        catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testValidTimeInFuture()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            populateTree(rtree);
            long now = ToolKit.currentSeconds();
            Spatial2DBiTemporalNowRelativeBinaryData future = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(TestObjectFactory.p2(), now-2200, now+400023, true, TestObjectFactory.byteData());
            rtree.add(future);

            Assert.assertNotNull(rtree);
            Observable<Spatial2DBiTemporalData<String>> observable = rtree.search(Geometryfactory.createBoundary(0, Long.MAX_VALUE, 0, Long.MAX_VALUE, 0, Constants.TRANSACTION_UC, now+200, now+500));
            count = 0;

            observable.subscribe(new DisposableObserver<Spatial2DBiTemporalData<String>>() {
                @Override
                public void onNext(Spatial2DBiTemporalData<String> data) {
                    System.out.print(data.UUID);
                    System.out.println(data.getClass());
                    count++;
                }

                @Override
                public void onError(Throwable e) {
                    System.out.print(e.getLocalizedMessage());
                }

                @Override
                public void onComplete() {
                    System.out.print("Done");
                }
            });

            Assert.assertTrue(count>= 1);
        }
        catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testdDeletePermanent()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            populateTree(rtree);
            long now = ToolKit.currentSeconds();
            Spatial2DBiTemporalNowRelativeBinaryData data = SpatioTemporalObjectFactory.createSpatial2DBiTemporalNowRelativeData(TestObjectFactory.p4(), now-150000, true, TestObjectFactory.byteData());

            System.out.print("UUID of Data to be deleted"+data.UUID);
            rtree.add(data);

            rtree.deletePermanent(data);

            Observable<Spatial2DBiTemporalData<String>> observable = rtree.search(Geometryfactory.createBoundary(data.X().start-20, data.X().start+20, data.Y().start-20, data.Y().start+20, 0, Constants.TRANSACTION_UC, now-200000, now));
            count = 0;

            DisposableObserver<Spatial2DBiTemporalData<String>> observer = new DisposableObserver<Spatial2DBiTemporalData<String>>() {
                @Override
                public void onNext(Spatial2DBiTemporalData<String> dta) {
                    System.out.println("UUID of data found:"+data.UUID);
                    Assert.assertNotEquals(data.UUID, data.UUID);
                }

                @Override
                public void onError(Throwable e) {
                    System.out.print(e.getLocalizedMessage());
                }

                @Override
                public void onComplete() {
                    System.out.print("Done");
                }
            };
            observable.subscribe(observer);

            observer.dispose();

        }
        catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testSearchByIdTestNonExistent()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");

            populateTree(rtree);

            Spatial2DBiTemporalData<String>[] data = new Spatial2DBiTemporalData[1];
            Single<Optional<Spatial2DBiTemporalData<String>>> single =rtree.searchByID("sjdkruh2448");
            DisposableSingleObserver<Optional<Spatial2DBiTemporalData<String>>> observer = new DisposableSingleObserver<Optional<Spatial2DBiTemporalData<String>>>() {
                @Override
                public void onSuccess(Optional<Spatial2DBiTemporalData<String>> sp2data) {
                    if(sp2data.isPresent())
                    {
                        Spatial2DBiTemporalData<String> d = sp2data.get();
                        if(!d.isDeleted())
                            data[0] = d;
                    }
                }

                @Override
                public void onError(Throwable e) {
                    Log.error(Log.getExceptionString(e));
                }
            };

            single.subscribeWith(observer);
            observer.dispose();
            Assert.assertNull(data[0]);
        }
        catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testAddVolume()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");
            int i = 50;

            while (i > 0)
            {
                populateTree(rtree);
                i--;
            }
            int size = rtree.size();
            Assert.assertNotNull(rtree.root);
        }catch (Exception ex)
        {
            Assert.fail();
        }
    }

    @Test
    public void testStreamAll()
    {
        try{
            ExtendedGRTree rtree = ExtendedGRTree.create("DC1007aDFg");
            int count = populateTree(rtree);

            Observable<Spatial2DBiTemporalData<String>> observable = rtree.streamAll();

            int[] expected = new int[1];
            DisposableObserver<Spatial2DBiTemporalData<String>> observer = new DisposableObserver<Spatial2DBiTemporalData<String>>() {
                @Override
                public void onNext(Spatial2DBiTemporalData<String> dta) {
                    expected[0] =  expected[0] +1;
                }

                @Override
                public void onError(Throwable e) {
                    System.out.print(e.getLocalizedMessage());
                }

                @Override
                public void onComplete() {
                    System.out.print("Done");
                }
            };

            observable.subscribe(observer);
            observer.dispose();

            Assert.assertEquals(count, expected[0]);
        }catch (Exception ex)
        {
            Assert.fail();
        }
    }

    private int populateTree(ExtendedGRTree rtree) throws Exception
    {
        HashMap<String, Spatial2DBiTemporalNowRelativeBinaryData> data = TestObjectFactory.dataSet();
        for (Spatial2DBiTemporalNowRelativeBinaryData d : data.values())
        {
            rtree.add(d);
        }
        return data.size();
    }



}
